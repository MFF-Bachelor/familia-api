<?php
declare(strict_types=1);

use Acelaya\Doctrine\Type\PhpEnumType;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\GenderEnum;
use App\Application\Model\Enum\NotificationTypeEnum;
use App\Application\Model\Enum\RecipeIngredientUnitEnum;
use App\Application\Model\Enum\RecipePreparationTimeUnitEnum;
use App\Application\Model\Enum\RelationshipEnum;
use App\Application\Model\Enum\RelationshipSideEnum;
use App\Application\Model\Enum\RelationshipStateEnum;
use App\Application\Model\Enum\TaskStateEnum;
use App\Application\Model\Enum\UserContactLabelEnum;
use App\Application\Model\Enum\UserContactTypeEnum;
use App\Application\Model\Enum\UserRecipeTypeEnum;
use DI\ContainerBuilder;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Kreait\Firebase\Factory;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Cache\FilesystemCache;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;


return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        LoggerInterface::class => function (ContainerInterface $container) {
            $settings = $container->get('settings');

            $loggerSettings = $settings['logger'];
            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },

        \Doctrine\DBAL\Connection::class => function (ContainerInterface $container) {
            $settings = $container->get('settings');

            $connectionParams = [
                'dbname'   => $settings['doctrine']['connection']['dbname'],
                'user'     => $settings['doctrine']['connection']['user'],
                'password' => $settings['doctrine']['connection']['password'],
                'host'     => $settings['doctrine']['connection']['host'],
                'driver'   => $settings['doctrine']['connection']['driver'],
            ];

            $connection = \Doctrine\DBAL\DriverManager::getConnection($connectionParams);
            $connection->connect();

            # Set up SQL logging
            $connection->getConfiguration()->setSQLLogger(new \Doctrine\DBAL\Logging\DebugStack());

            return $connection;
        },

        \Kreait\Firebase\Auth::class => function (ContainerInterface $container) {
            //get firebase credentials filename from settings
            $settings = $container->get('settings');
            $firebaseCredentials = $settings['firebaseCredentials'];

            $factory = (new Factory)->withServiceAccount(dirname(__DIR__, 1) . $firebaseCredentials);

            return $factory->createAuth();

        },

        \Kreait\Firebase\Messaging::class => function (ContainerInterface $container) {
            //get firebase credentials filename from settings
            $settings = $container->get('settings');
            $firebaseCredentials = $settings['firebaseCredentials'];

            $factory = (new Factory)->withServiceAccount(dirname(__DIR__, 1) . $firebaseCredentials);

            return $factory->createMessaging();

        },

        EntityManagerInterface::class => function (ContainerInterface $c): EntityManager {
            $doctrineSettings = $c->get('settings')['doctrine'];

            $config = Setup::createAnnotationMetadataConfiguration(
                $doctrineSettings['metadata_dirs'],
                $doctrineSettings['dev_mode'],
                null,
                null,
                false
            );

            AnnotationReader::addGlobalIgnoredName('TheCodingMachine\GraphQLite\Annotations\Type');
            AnnotationReader::addGlobalIgnoredName('TheCodingMachine\GraphQLite\Annotations\Field');

            $config->setMetadataDriverImpl(
                new AnnotationDriver(
                    new AnnotationReader,
                    $doctrineSettings['metadata_dirs']
                )
            );

            $config->setMetadataCacheImpl(
                new FilesystemCache($doctrineSettings['cache_dir'])
            );

            $config->addCustomStringFunction("COLLATE", "App\\DoctrineExtensions\\CollateFunction");

            $em = EntityManager::create($doctrineSettings['connection'], $config);

            //Registration of enum types
            PhpEnumType::registerEnumTypes([
                'entity_state_enum' => EntityStateEnum::class,
                'gender_enum' => GenderEnum::class,
                'relationship_enum' => RelationshipEnum::class,
                'relationship_state_enum' => RelationshipStateEnum::class,
                'relationship_side_enum' => RelationshipSideEnum::class,
                'notification_type_enum' => NotificationTypeEnum::class,
                'task_state_enum' => TaskStateEnum::class,
                'recipe_preparation_time_unit_enum' => RecipePreparationTimeUnitEnum::class,
                'recipe_ingredient_unit_enum' => RecipeIngredientUnitEnum::class,
                'user_recipe_type_enum' => UserRecipeTypeEnum::class,
                'user_contact_type_enum' => UserContactTypeEnum::class,
                'user_contact_label_enum' => UserContactLabelEnum::class,
            ]);
            $platform = $em->getConnection()->getDatabasePlatform();
            $platform->registerDoctrineTypeMapping('enum', 'string');

            $platform->registerDoctrineTypeMapping('VARCHAR', 'entity_state_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'gender_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'relationship_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'relationship_state_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'relationship_side_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'notification_type_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'task_state_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'recipe_preparation_time_unit_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'recipe_ingredient_unit_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'user_recipe_type_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'user_contact_type_enum');
            $platform->registerDoctrineTypeMapping('VARCHAR', 'user_contact_label_enum');



            return $em;
        }
    ]);
};