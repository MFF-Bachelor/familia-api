<?php


namespace App\DoctrineExtensions;


use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

class CollateFunction extends FunctionNode
{
    public $expressionToCollate = null;
    public $collation = null;
    public $withApostrophe = false;

    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->expressionToCollate = $parser->StringPrimary();

        $parser->match(Lexer::T_COMMA);

        $parser->match(Lexer::T_IDENTIFIER);
        $this->collation = $parser->getLexer()->token['value'];

        $parser->match(Lexer::T_COMMA);

        $parser->match(Lexer::T_IDENTIFIER);
        $lexer = $parser->getLexer();
        $this->withApostrophe = $lexer->token['value'];

        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(SqlWalker $sqlWalker)
    {
        if ($this->withApostrophe == 'true') {
            $result = $this->expressionToCollate->dispatch($sqlWalker);
        } else {
            $result = $this->expressionToCollate->dispatch($sqlWalker);
            $result = $FileName = str_replace("'", "", $result);
        }
        return sprintf( '%s COLLATE %s', $result, $this->collation );
    }
}