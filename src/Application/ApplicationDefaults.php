<?php


namespace App\Application;

// Definition of tables of entities in application
class ApplicationDefaults
{
    public static $userTable = "App\\Application\\Model\\Entities\\User";
    public static $relationshipTable = "App\\Application\\Model\\Entities\\Relationship";
    public static $inactiveUserTable = "App\\Application\\Model\\Entities\\InactiveUser";
    public static $notificationTable = "App\\Application\\Model\\Entities\\Notification";
    public static $fcmTokenTable = "App\\Application\\Model\\Entities\\FcmToken";
    public static $sentNotificationTable = "App\\Application\\Model\\Entities\\SentNotification";
    public static $taskTable = "App\\Application\\Model\\Entities\\Task";
    public static $recipeTable = "App\\Application\\Model\\Entities\\Recipe";
    public static $userRecipeTable = "App\\Application\\Model\\Entities\\UserRecipe";
    public static $recipeCategoryTable = "App\\Application\\Model\\Entities\\RecipeCategory";
    public static $userContactTable = "App\\Application\\Model\\Entities\\UserContact";
    public static $userEmailTable = "App\\Application\\Model\\Entities\\UserEmail";
    public static $shoppingListTable = "App\\Application\\Model\\Entities\\ShoppingList";
    public static $shoppingListItemTable = "App\\Application\\Model\\Entities\\ShoppingListItem";
}