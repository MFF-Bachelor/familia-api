<?php

namespace App\Application\Services\MediaService;

use RuntimeException;

class MediaServiceException extends RuntimeException
{
    const CODE_GENERIC_SERVER_ERROR = 0;
    const CODE_CONFIG_ERROR = 1;
    const CODE_INVALID_IMAGE_FORMAT = 2;
    const CODE_IMAGE_NOT_FOUND = 3;
    const CODE_INVALID_INPUT_IMAGE = 4;
}