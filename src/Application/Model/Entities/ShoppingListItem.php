<?php


namespace App\Application\Model\Entities;

use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type(name="ShoppingListItem")
 *
 * Shopping list item entity
 *
 * ORM annotations:
 * @ORM\Table(name="shopping_list_item")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ShoppingListItem
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    private $title;

    /**
     * @var ShoppingList
     *
     * @ORM\ManyToOne(targetEntity="ShoppingList", inversedBy="shoppingListItems", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="shopping_list_id", referencedColumnName="id")
     * })
     */
    private $shoppingList;

    /**
     * @var bool
     *
     * @ORM\Column(name="checked", type="boolean", nullable=false)
     */
    private $checked;

    /**
     * @param string|null $title
     * @param bool $checked
     * @return ShoppingListItem
     */
    public static function create(
        string $title = null,
        bool $checked = false
    ) : ShoppingListItem {
        $instance = new self();

        $instance->title = $title;
        $instance->checked = $checked;

        return $instance;
    }

    /**
     * @Field()
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @Field()
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @Field()
     *
     * @return ShoppingList
     */
    public function getShoppingList(): ShoppingList
    {
        return $this->shoppingList;
    }

    /**
     * @param ShoppingList $shoppingList
     */
    public function setShoppingList(ShoppingList $shoppingList): void
    {
        $this->shoppingList = $shoppingList;
    }

    /**
     * @Field()
     *
     * @return bool
     */
    public function isChecked(): bool
    {
        return $this->checked;
    }

    /**
     * @param bool $checked
     */
    public function setChecked(bool $checked): void
    {
        $this->checked = $checked;
    }
}