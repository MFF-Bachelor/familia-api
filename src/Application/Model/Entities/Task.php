<?php

namespace App\Application\Model\Entities;

use App\Application\Model\Enum\TaskStateEnum;
use App\Application\Model\Timestampable;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;


/** GraphQLite annotations:
 * @Type(name="Task")
 *
 * Task entity
 *
 * ORM annotations:
 * @ORM\Table(name="task")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Task {
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="text", type="text", nullable=true)
     */
    private $text;

    /**
     * @var int
     *
     * @ORM\Column(name="priority", type="text", nullable=true)
     */
    private $priority;

    /**
     * @var DateTimeImmutable|null
     * @ORM\Column(name="due_time", type="datetime", nullable=true)
     */
    protected $dueTime;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="createdTasks", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="creator_id", referencedColumnName="id")
     * })
     */
    private $creator;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="assigneeTasks", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="assignee_id", referencedColumnName="id")
     * })
     */
    private $assignee;

    /**
     * @var TaskStateEnum
     *
     * @ORM\Column(name="state", type="task_state_enum", nullable=false)
     */
    private $state;

    /**
     * @var bool
     *
     * @ORM\Column(name="can_assignee_edit", type="boolean", nullable=false)
     */
    private $canAssigneeEdit;

    /**
     * @param string $title
     * @param string $text
     * @param DateTimeImmutable|null $dueTime
     * @param User $creator
     * @param User $assignee
     * @param int $priority
     * @param TaskStateEnum $state
     * @return Task
     */
    public static function create(
        string $title,
        string $text,
        ?DateTimeImmutable $dueTime,
        User $creator,
        User $assignee,
        int $priority,
        TaskStateEnum $state,
        bool $canAssigneeEdit
    ) : Task {
        $instance = new self();

        $instance->title = $title;
        $instance->text = $text;
        $instance->dueTime = $dueTime;
        $instance->creator = $creator;
        $instance->assignee = $assignee;
        $instance->priority = $priority;
        $instance->state = $state;
        $instance->canAssigneeEdit = $canAssigneeEdit;

        return $instance;
    }

    /**
     * @Field()
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @Field()
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @Field()
     *
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     */
    public function setText(?string $text): void
    {
        $this->text = $text;
    }

    /**
     * @Field()
     *
     * @return DateTimeImmutable|null
     */
    public function getDueTime(): ?DateTimeImmutable
    {
        if ($this->dueTime == null)
            return null;
        return new DateTimeImmutable($this->dueTime->format(DateTimeImmutable::ATOM));
    }

    /**
     * @param DateTimeImmutable|null $dueTime
     */
    public function setDueTime(?DateTimeImmutable $dueTime): void
    {
        $this->dueTime = $dueTime;
    }

    /**
     * @Field()
     *
     * @return User
     */
    public function getCreator(): User
    {
        return $this->creator;
    }

    /**
     * @param User $creator
     */
    public function setCreator(User $creator): void
    {
        $this->creator = $creator;
    }

    /**
     * @Field()
     *
     * @return User
     */
    public function getAssignee(): User
    {
        return $this->assignee;
    }

    /**
     * @param User $assignee
     */
    public function setAssignee(User $assignee): void
    {
        $this->assignee = $assignee;
    }

    /**
     * @Field()
     *
     * @return TaskStateEnum
     */
    public function getState(): TaskStateEnum
    {
        return $this->state;
    }

    /**
     * @param TaskStateEnum $state
     */
    public function setState(TaskStateEnum $state): void
    {
        $this->state = $state;
    }

    /**
     * @Field()
     *
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     */
    public function setPriority(int $priority): void
    {
        $this->priority = $priority;
    }

    /**
     * @Field()
     *
     * @return bool
     */
    public function getCanAssigneeEdit(): bool
    {
        return $this->canAssigneeEdit;
    }

    /**
     * @param bool $canAssigneeEdit
     */
    public function setCanAssigneeEdit(bool $canAssigneeEdit): void
    {
        $this->canAssigneeEdit = $canAssigneeEdit;
    }
}