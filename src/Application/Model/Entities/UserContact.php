<?php


namespace App\Application\Model\Entities;


use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\UserContactLabelEnum;
use App\Application\Model\Enum\UserContactTypeEnum;
use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;


/** GraphQLite annotations:
 * @Type(name="UserContact")
 *
 * UserContact entity
 *
 * @ORM\Table(name="user_contact")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class UserContact
{

    #region private fields

    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="contacts", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var string|null
     *
     * @ORM\Column(name="email", type="string", nullable=true)
     */
    private $email;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone_number", type="string", nullable=true)
     */
    private $phoneNumber;

    /**
     * @var UserContactLabelEnum
     *
     * @ORM\Column(name="label", type="user_contact_label_enum", nullable=false)
     */
    private $label;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    private $name;


    /**
     * @var EntityStateEnum
     *
     * @ORM\Column(name="state", type="entity_state_enum", nullable=false)
     */
    private $state;

    #endregion

    #region constructor //provided by create function

    /**
     * @param User $user
     * @param string|null $email
     * @param string|null $phoneNumber
     * @param UserContactLabelEnum $label
     * @param string $name
     * @param EntityStateEnum $state
     * @return UserContact
     */
    public static function create
    (
        User $user,
        ?string $email,
        ?string $phoneNumber,
        UserContactLabelEnum $label,
        string $name,
        EntityStateEnum $state
    ) : UserContact {
        $instance = new self();

        $instance->user = $user;
        $instance->email = $email;
        $instance->phoneNumber = $phoneNumber;
        $instance->label = $label;
        $instance->name = $name;
        $instance->state = $state;

        return $instance;
    }

    #endregion

    #region getters and setters

    /**
     * @Field()
     *
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @Field()
     *
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @Field()
     *
     * @return UserContactLabelEnum
     */
    public function getLabel(): UserContactLabelEnum
    {
        return $this->label;
    }

    /**
     * @param UserContactLabelEnum $label
     */
    public function setLabel(UserContactLabelEnum $label): void
    {
        $this->label = $label;
    }

    /**
     * @Field()
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @Field()
     *
     * @return EntityStateEnum
     */
    public function getState(): EntityStateEnum
    {
        return $this->state;
    }

    /**
     * @param EntityStateEnum $state
     */
    public function setState(EntityStateEnum $state): void
    {
        $this->state = $state;
    }

    #endregion

    /**
     * @Field()
     *
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @Field()
     *
     * @return string|null
     */
    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    /**
     * @param string|null $phoneNumber
     */
    public function setPhoneNumber(?string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

}