<?php

namespace App\Application\Model\Entities;

use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\GenderEnum;
use App\Application\Model\Timestampable;
use DateTimeImmutable;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\ORMException;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations
 * @Type(name="InactiveUser")
 *
 * InactiveUser entity
 *
 * @ORM\Table(name="inactive_user")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class InactiveUser
{

    #region private fields

    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var EntityStateEnum
     *
     * @ORM\Column(name="state", type="entity_state_enum", nullable=false)
     */
    private $state;

    /**
     * @var string|null
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     */
    private $lastName;


    /**
     * @var DateTimeImmutable|null
     * @ORM\Column(name="birth_date", type="datetime", nullable=true)
     */
    protected $birthDate;

    /**
     * @var GenderEnum|null
     *
     * @ORM\Column(name="gender", type="gender_enum", nullable=true)
     */
    private $gender;
    #endregion

    #region constructor //provided by create function

    /**
     * User constructor.
     * @param EntityStateEnum $state
     * @param string|null $firstName
     * @param string|null $lastName
     * @param DateTimeImmutable|null $birthDate
     * @param GenderEnum|null $gender
     * @param EntityManager $em
     * @return InactiveUser
     * @throws ORMException
     */
    public static function create(
        EntityStateEnum $state,
        string $firstName = null,
        string $lastName = null,
        DateTimeImmutable $birthDate = null,
        GenderEnum $gender = null,
        EntityManager $em
    ) : InactiveUser
    {
        $instance = new self();

        $instance->state = $state;
        $instance->firstName = $firstName;
        $instance->lastName = $lastName;
        $instance->birthDate = $birthDate;
        $instance->gender = $gender;

        return $instance;
    }

    #endregion

    #region getters and setters

    /**
     * @Field()
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @Field()
     *
     * @return EntityStateEnum
     */
    public function getState(): EntityStateEnum
    {
        return $this->state;
    }

    /**
     * @param EntityStateEnum $state
     */
    public function setState(EntityStateEnum $state): void
    {
        $this->state = $state;
    }

    /**
     * @Field()
     *
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     */
    public function setFirstName(?string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @Field()
     *
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     */
    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @Field()
     *
     * @return DateTimeImmutable|null
     * @throws Exception
     */
    public function getBirthDate(): ?DateTimeImmutable
    {
        if (is_null($this->birthDate))
            return null;
        return new DateTimeImmutable($this->birthDate->format(DateTimeImmutable::ATOM));;
    }

    /**
     * @param DateTimeImmutable|null $birthDate
     */
    public function setBirthDate(?DateTimeImmutable $birthDate): void
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @Field()
     *
     * @return GenderEnum|null
     */
    public function getGender(): ?GenderEnum
    {
        return $this->gender;
    }

    /**
     * @param GenderEnum|null $gender
     */
    public function setGender(?GenderEnum $gender): void
    {
        $this->gender = $gender;
    }


}
