<?php


namespace App\Application\Model\Entities;

use App\Application\Model\Timestampable;
use DateTimeImmutable;
use DateTimeZone;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type(name="SentNotification")
 *
 * SentNotification entity
 *
 * ORM annotations:
 * @ORM\Table(name="sent_notification")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class SentNotification
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var FcmToken
     *
     * @ORM\ManyToOne(targetEntity="FcmToken", inversedBy="sentNotifications", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="fcm_token_id", referencedColumnName="id")
     * })
     */
    private $fcmToken;

    /**
     * @var Notification
     *
     * @ORM\ManyToOne(targetEntity="Notification", inversedBy="sentNotifications", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="notification_id", referencedColumnName="id")
     * })
     */
    private $notification;

    /**
     * @var DateTimeImmutable|null
     *
     * @ORM\Column(name="send_time", type="datetime", nullable=true)
     */
    private $sendTime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="error_message", type="string", nullable=true)
     */
    private $errorMessage;

    /**
     * SentNotification constructor.
     * @param FcmToken $fcmToken
     * @param Notification $notification
     * @param string|null $errorMessage
     * @return SentNotification
     * @throws Exception
     */
    public static function create(
        FcmToken $fcmToken,
        Notification $notification,
        ?string $errorMessage
    ): SentNotification
    {
        $instance = new self();

        $instance->fcmToken = $fcmToken;
        $instance->notification = $notification;
        $instance->sendTime = new DateTimeImmutable('now', new DateTimeZone('UTC'));
        $instance->errorMessage = $errorMessage;

        return $instance;
    }

    /**
     * @Field()
     *
     * @return FcmToken
     */
    public function getFcmToken(): FcmToken
    {
        return $this->fcmToken;
    }

    /**
     * @param FcmToken $fcmToken
     */
    public function setFcmToken(FcmToken $fcmToken): void
    {
        $this->fcmToken = $fcmToken;
    }

    /**
     * @Field()
     *
     * @return Notification
     */
    public function getNotification(): Notification
    {
        return $this->notification;
    }

    /**
     * @param Notification $notification
     */
    public function setNotification(Notification $notification): void
    {
        $this->notification = $notification;
    }

    /**
     * @Field()
     *
     * @return DateTimeImmutable|null
     * @throws Exception
     */
    public function getSendTime(): ?DateTimeImmutable
    {
        if ($this->created == null)
            return null;
        return new DateTimeImmutable($this->created->format(DateTimeImmutable::ATOM));
    }

    /**
     * @param DateTimeImmutable|null $sendTime
     */
    public function setSendTime(?DateTimeImmutable $sendTime): void
    {
        $this->sendTime = $sendTime;
    }

    /**
     * @Field()
     *
     * @return string|null
     */
    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }

    /**
     * @param string|null $errorMessage
     */
    public function setErrorMessage(?string $errorMessage): void
    {
        $this->errorMessage = $errorMessage;
    }
}