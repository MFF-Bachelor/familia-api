<?php

namespace App\Application\Model\Entities;

use App\Application\Model\Enum\RecipePreparationTimeUnitEnum;
use App\Application\Model\Enum\TaskStateEnum;
use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\ManyToMany;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;


/** GraphQLite annotations:
 * @Type(name="RecipeCategory")
 *
 * RecipeCategory entity
 *
 * ORM annotations:
 * @ORM\Table(name="recipe_category")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class RecipeCategory {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /** RecipeSubCategories of this RecipeCategory
     *
     * @var RecipeCategory[]|null
     *
     * @ORM\OneToMany(targetEntity="RecipeCategory", mappedBy="parentRecipeCategory", cascade={"persist", "remove"})
     */
    private $recipeSubCategories;


    /** Parent RecipeCategory
     *
     * @var RecipeCategory|null
     *
     * @ORM\ManyToOne(targetEntity="RecipeCategory", inversedBy="recipeSubCategories")
     * @ORM\JoinColumn(name="parent_recipe_category", referencedColumnName="id")
     */
    private $parentRecipeCategory;


    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    private $name;

    /**
     * @var Recipe[]|null
     *
     * @ManyToMany(targetEntity="Recipe", mappedBy="recipeCategories", indexBy="id", fetch="EAGER")
     */
    private $recipes;


    /**
     * @param RecipeCategory $parentRecipeCategory
     * @param string|null $name
     * @return RecipeCategory
     */
    public static function create(
        RecipeCategory $parentRecipeCategory,
        string $name = null
    ) : RecipeCategory {
        $instance = new self();

        $instance->parentRecipeCategory = $parentRecipeCategory;
        $instance->name = $name;

        return $instance;
    }

    /**
     * @Field
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @Field
     *
     * @return RecipeCategory[]|null
     */
    public function getRecipeSubCategories(): ?iterable
    {
        return $this->recipeSubCategories;
    }

    /**
     * @param RecipeCategory[]|null $recipeSubCategories
     */
    public function setRecipeSubCategories(?array $recipeSubCategories): void
    {
        $this->recipeSubCategories = $recipeSubCategories;
    }

    /**
     * @Field
     *
     * @return RecipeCategory|null
     */
    public function getParentRecipeCategory(): ?RecipeCategory
    {
        return $this->parentRecipeCategory;
    }

    /**
     * @param RecipeCategory|null $parentRecipeCategory
     */
    public function setParentRecipeCategory(?RecipeCategory $parentRecipeCategory): void
    {
        $this->parentRecipeCategory = $parentRecipeCategory;
    }

    /**
     * @Field
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    public function removeRecipe(Recipe $recipe) {
        $this->recipes->removeElement($recipe);
    }

}