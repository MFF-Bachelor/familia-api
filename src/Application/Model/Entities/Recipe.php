<?php

namespace App\Application\Model\Entities;

use App\Application\Model\Enum\RecipePreparationTimeUnitEnum;
use App\Application\Model\Timestampable;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\ORMException;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;


/** GraphQLite annotations:
 * @Type(name="Recipe")
 *
 * Recipe entity
 *
 * ORM annotations:
 * @ORM\Table(name="recipe")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Recipe
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text", nullable=false)
     */
    private $title;

    /**
     * @var int|null
     *
     * @ORM\Column(name="servings", type="integer", nullable=true)
     */
    private $servings;

    /**
     * @var int|null
     *
     * @ORM\Column(name="calories_per_serving", type="integer", nullable=true)
     */
    private $caloriesPerServing;

    /**
     * @var string|null
     *
     * @ORM\Column(name="source", type="text", nullable=true)
     */
    private $source;

    /**
     * @var int|null
     *
     * @ORM\Column(name="preparation_time", type="integer", nullable=true)
     */
    private $preparationTime;

    /**
     * @var RecipePreparationTimeUnitEnum|null
     *
     * @ORM\Column(name="preparation_time_unit", type="recipe_preparation_time_unit_enum", nullable=true)
     */
    private $preparationTimeUnit;

    /**
     * @var int|null
     *
     * @ORM\Column(name="difficulty", type="integer", nullable=true)
     */
    private $difficulty;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var RecipeCategory[]|null
     *
     * Many Recipes have many RecipeCategories
     * @ORM\ManyToMany(targetEntity="RecipeCategory", inversedBy="recipes")
     * @ORM\JoinTable(name="recipe_recipe_category",
     *      joinColumns={@ORM\JoinColumn(name="recipe_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="recipe_category_id", referencedColumnName="id")}
     * )
     */
    private $recipeCategories;

    /**
     * @var RecipeIngredient[]|null
     *
     * @ORM\OneToMany(targetEntity="RecipeIngredient", mappedBy="recipe", cascade={"persist", "remove"})
     */
    private $recipeIngredients;

    /**
     * @var RecipeInstruction[]|null
     *
     * @ORM\OneToMany(targetEntity="RecipeInstruction", mappedBy="recipe", cascade={"persist", "remove"})
     */
    private $recipeInstructions;

    /**
     * @var UserRecipe[]|null
     *
     * Connection to entity UserRecipe in which is it possible to enable User
     *
     * @ORM\OneToMany(targetEntity="UserRecipe", mappedBy="recipe", cascade={"persist", "remove"})
     */
    private $recipeUsers;


    /**
     * @param string $title
     * @param int|null $servings
     * @param int|null $caloriesPerServing
     * @param string|null $source
     * @param int|null $preparationTime
     * @param RecipePreparationTimeUnitEnum|null $preparationTimeUnit
     * @param int|null $difficulty
     * @param string|null $description
     * @param RecipeIngredient[]|null $recipeIngredients
     * @param RecipeInstruction[]|null $recipeInstructions
     * @param array|null $recipeCategories
     * @param EntityManager $em
     * @return Recipe
     * @throws ORMException
     */
    public static function create(
        string $title,
        int $servings = null,
        int $caloriesPerServing = null,
        string $source = null,
        int $preparationTime = null,
        RecipePreparationTimeUnitEnum $preparationTimeUnit = null,
        int $difficulty = null,
        string $description = null,
        ?array $recipeIngredients,
        ?array $recipeInstructions,
        ?array $recipeCategories,
        EntityManager $em
    ) : Recipe {
        $instance = new self();

        $instance->title = $title;
        $instance->servings = $servings;
        $instance->caloriesPerServing = $caloriesPerServing;
        $instance->source = $source;
        $instance->preparationTime = $preparationTime;
        $instance->preparationTimeUnit = $preparationTimeUnit;
        $instance->difficulty = $difficulty;
        $instance->description = $description;
        $instance->setRecipeIngredients($recipeIngredients, $em);
        $instance->setRecipeInstructions($recipeInstructions, $em);
        $instance->setRecipeCategories($recipeCategories, $em);

        return $instance;
    }

    /**
     * @param RecipeIngredient[]|null $recipeIngredients
     * @param EntityManager $em
     * @throws ORMException
     */
    public function setRecipeIngredients(?array $recipeIngredients, EntityManager $em): void
    {
        if (is_array($this->recipeIngredients) || is_object($this->recipeIngredients))
            foreach ($this->recipeIngredients as $recipeIngredient) {
                $em->remove($recipeIngredient);
                unset($recipeIngredient);
            }
        if (is_array($recipeIngredients) || is_object($recipeIngredients))
            foreach ($recipeIngredients as $recipeIngredient) {
                $recipeIngredient->setRecipe($this);
                $this->recipeIngredients[] = $recipeIngredient;
            }
    }

    /**
     * @param RecipeInstruction[]|null $recipeInstructions
     * @param EntityManager $em
     * @throws ORMException
     */
    public function setRecipeInstructions(?array $recipeInstructions, EntityManager $em): void
    {
        if (is_array($this->recipeInstructions) || is_object($this->recipeInstructions))
            foreach ($this->recipeInstructions as $recipeInstruction) {
                $em->remove($recipeInstruction);
                unset($recipeInstruction);
            }
        if (is_array($recipeInstructions) || is_object($recipeInstructions))
            foreach ($recipeInstructions as $recipeInstruction) {
                $recipeInstruction->setRecipe($this);
                $this->recipeInstructions[] = $recipeInstruction;
            }
    }

    /**
     * @Field
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @Field
     *
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @Field
     *
     * @return int|null
     */
    public function getServings(): ?int
    {
        return $this->servings;
    }

    /**
     * @param int|null $servings
     */
    public function setServings(?int $servings): void
    {
        $this->servings = $servings;
    }

    /**
     * @Field
     *
     * @return int|null
     */
    public function getCaloriesPerServing(): ?int
    {
        return $this->caloriesPerServing;
    }

    /**
     * @param int|null $caloriesPerServing
     */
    public function setCaloriesPerServing(?int $caloriesPerServing): void
    {
        $this->caloriesPerServing = $caloriesPerServing;
    }

    /**
     * @Field
     *
     * @return string|null
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * @param string|null $source
     */
    public function setSource(?string $source): void
    {
        $this->source = $source;
    }

    /**
     * @Field
     *
     * @return int|null
     */
    public function getPreparationTime(): ?int
    {
        return $this->preparationTime;
    }

    /**
     * @param int|null $preparationTime
     */
    public function setPreparationTime(?int $preparationTime): void
    {
        $this->preparationTime = $preparationTime;
    }

    /**
     * @Field
     *
     * @return RecipePreparationTimeUnitEnum|null
     */
    public function getPreparationTimeUnit(): ?RecipePreparationTimeUnitEnum
    {
        return $this->preparationTimeUnit;
    }

    /**
     * @param RecipePreparationTimeUnitEnum|null $preparationTimeUnit
     */
    public function setPreparationTimeUnit(?RecipePreparationTimeUnitEnum $preparationTimeUnit): void
    {
        $this->preparationTimeUnit = $preparationTimeUnit;
    }

    /**
     * @Field
     *
     * @return int|null
     */
    public function getDifficulty(): ?int
    {
        return $this->difficulty;
    }

    /**
     * @param int|null $difficulty
     */
    public function setDifficulty(?int $difficulty): void
    {
        $this->difficulty = $difficulty;
    }

    /**
     * @Field
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @Field
     *
     * @return RecipeCategory[]|null
     */
    public function getRecipeCategories(): ?iterable
    {
        return $this->recipeCategories;
    }

    /**
     * @param RecipeCategory[]|null $recipeCategories
     * @param EntityManager $em
     */
    public function setRecipeCategories(?array $recipeCategories, EntityManager $em): void
    {
        if (is_array($this->recipeCategories) || is_object($this->recipeCategories))
            foreach ($this->recipeCategories as $recipeCategory) {
                $recipeCategory->removeRecipe($this);
                $this->recipeCategories->removeElement($recipeCategory);
            }
        if (is_array($recipeCategories) || is_object($recipeCategories))
            foreach ($recipeCategories as $recipeCategory) {
//                $recipeCategory->setRecipe($this);
                $this->recipeCategories[] = $recipeCategory;
            }
    }


    /**
     * @Field
     *
     * @return RecipeIngredient[]|null
     */
    public function getRecipeIngredients(): ?iterable
    {
        return $this->recipeIngredients;
    }

    /**
     * @Field
     *
     * @return RecipeInstruction[]|null
     */
    public function getRecipeInstructions(): ?iterable
    {
        return $this->recipeInstructions;
    }

    /**
     * @Field
     *
     * @return UserRecipe[]|null
     */
    public function getRecipeUsers(): ?iterable
    {
        return $this->recipeUsers;
    }

    /**
     * @param UserRecipe[]|null $recipeUsers
     */
    public function setRecipeUsers(?array $recipeUsers): void
    {
        $this->recipeUsers = $recipeUsers;
    }


}