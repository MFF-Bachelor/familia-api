<?php

namespace App\Application\Model\Entities;

use App\Application\Model\Enum\RecipeIngredientUnitEnum;
use App\Application\Model\Enum\RecipePreparationTimeUnitEnum;
use App\Application\Model\Enum\TaskStateEnum;
use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;


/** GraphQLite annotations:
 * @Type(name="RecipeIngredient")
 *
 * RecipeIngredient entity
 *
 * ORM annotations:
 * @ORM\Table(name="recipe_ingredient")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class RecipeIngredient {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Recipe
     *
     * @ORM\ManyToOne(targetEntity="Recipe", inversedBy="recipe_ingredients", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="recipe_id", referencedColumnName="id")
     * })
     */
    private $recipe;

    /**
     * @var int|null
     *
     * @ORM\Column(name="amount", type="integer", nullable=true)
     */
    private $amount;

    /**
     * @var RecipeIngredientUnitEnum|null
     *
     * @ORM\Column(name="unit", type="recipe_ingredient_unit_enum", nullable=true)
     */
    private $unit;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    private $name;

    /**
     * @param int|null $amount
     * @param RecipeIngredientUnitEnum|null $unit
     * @param string|null $name
     * @return RecipeIngredient
     */
    public static function create(
        int $amount = null,
        RecipeIngredientUnitEnum $unit = null,
        string $name = null
    ) : RecipeIngredient {
        $instance = new self();

        $instance->amount = $amount;
        $instance->unit = $unit;
        $instance->name = $name;

        return $instance;
    }

    /**
     * @Field
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @Field
     *
     * @return Recipe
     */
    public function getRecipe(): Recipe
    {
        return $this->recipe;
    }

    /**
     * @param Recipe|null $recipe
     */
    public function setRecipe(?Recipe $recipe): void
    {
        $this->recipe = $recipe;
    }

    /**
     * @Field
     *
     * @return int|null
     */
    public function getAmount(): ?int
    {
        return $this->amount;
    }

    /**
     * @param int|null $amount
     */
    public function setAmount(?int $amount): void
    {
        $this->amount = $amount;
    }

    /**
     * @Field
     *
     * @return RecipeIngredientUnitEnum|null
     */
    public function getUnit(): ?RecipeIngredientUnitEnum
    {
        return $this->unit;
    }

    /**
     * @param RecipeIngredientUnitEnum|null $unit
     */
    public function setUnit(?RecipeIngredientUnitEnum $unit): void
    {
        $this->unit = $unit;
    }

    /**
     * @Field
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }
}