<?php


namespace App\Application\Model\Entities;

use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type(name="FcmToken")
 *
 * FcmToken Entity
 *
 * ORM annotations:
 * @ORM\Table(name="fcm_token")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class FcmToken
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="fcm_token", type="string", nullable=false)
     */
    private $fcmToken;

    /**
     * @var int
     * @ORM\Column(name="user_id", type="integer", nullable=false)
     */
    private $userId;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="fcmTokens", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /** Sent notifications
     *
     * @var SentNotification[]|null
     *
     * @ORM\OneToMany(targetEntity="SentNotification", mappedBy="fcmToken", cascade={"persist", "remove"})
     */
    private $sentNotifications;


    /**
     * FcmToken constructor.
     * @param User $user
     * @param string $fcmToken
     * @return FcmToken
     */
    public static function create(
        User $user,
        string $fcmToken
    ): FcmToken
    {
        $instance = new self();

        $instance->user = $user;
        $instance->fcmToken = $fcmToken;
        $instance->userId = $user->getId();

        return $instance;
    }

    /**
     * @Field()
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @Field()
     *
     * @return string
     */
    public function getFcmToken(): string
    {
        return $this->fcmToken;
    }

    /**
     * @param string $fcmToken
     */
    public function setFcmToken(string $fcmToken): void
    {
        $this->fcmToken = $fcmToken;
    }

    /**
     * @Field()
     *
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @Field()
     *
     * @return SentNotification[]|null
     */
    public function getSentNotifications(): ?iterable
    {
        return $this->sentNotifications;
    }
}