<?php


namespace App\Application\Model\Entities;


use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type(name="UserPhoneNumber")
 *
 * UserPhoneNumber entity
 *
 * ORM annotations:
 * @ORM\Table(name="user_phone_number")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class UserPhoneNumber
{
    #region private fields
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="phone_number", type="string", length=255, nullable=false)
     */
    private $phoneNumber;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="phoneNumbers", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;
    #endregion

    #region constructor //provided by create function

    /**
     * @param User $user
     * @param string $phoneNumber
     *
     * @return UserPhoneNumber
     */
    public static function create(User $user, string $phoneNumber) : UserPhoneNumber {
        $instance = new self();

        $instance->user = $user;
        $instance->phoneNumber = $phoneNumber;

        return $instance;
    }

    #endregion

    #region getters and setters

    /**
     * @Field()
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @Field()
     *
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber(string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @Field()
     *
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    #endregion
}