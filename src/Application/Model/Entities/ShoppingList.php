<?php


namespace App\Application\Model\Entities;


use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Timestampable;
use DateTimeImmutable;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\ORMException;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type(name="ShoppingList")
 *
 * Shopping list entity
 *
 * ORM annotations:
 * @ORM\Table(name="shopping_list")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ShoppingList
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="text", nullable=true)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", nullable=true)
     */
    private $description;

    /**
     * @var DateTimeImmutable|null
     * @ORM\Column(name="due_time", type="datetime", nullable=true)
     */
    protected $dueTime;

    /**
     * @var User|null
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="createdShoppingLists", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="creator_id", referencedColumnName="id")
     * })
     */
    private $creator;

    /**
     * @var User|null
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="assigneeShoppingLists",  cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="assignee_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $assignee;

    /**
     * @var ShoppingListItem[]|null
     *
     * @ORM\OneToMany(targetEntity="ShoppingListItem", mappedBy="shoppingList", cascade={"persist", "remove"})
     */
    private $shoppingListItems;

    /**
     * @var EntityStateEnum
     *
     * @ORM\Column(name="state", type="entity_state_enum", nullable=false)
     */
    private $state;


    /**
     * @param string $title
     * @param string $description
     * @param ShoppingListItem[]|null $shoppingListItems
     * @param DateTimeImmutable|null $dueTime
     * @param User $creator
     * @param User|null $assignee
     * @param EntityStateEnum $state
     * @param EntityManager $em
     * @return ShoppingList
     * @throws ORMException
     */
    public static function create(
        string $title,
        string $description,
        ?array $shoppingListItems,
        ?DateTimeImmutable $dueTime,
        User $creator,
        ?User $assignee,
        EntityStateEnum $state,
        EntityManager $em
    ) : ShoppingList {
        $instance = new self();

        $instance->title = $title;
        $instance->description = $description;
        $instance->dueTime = $dueTime;
        $instance->creator = $creator;
        $instance->assignee = $assignee;
        $instance->setShoppingListItems($shoppingListItems, $em);
        $instance->state = $state;

        return $instance;
    }

    /**
     * @Field()
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @Field()
     *
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @Field()
     *
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @Field()
     *
     * @return DateTimeImmutable|null
     */
    public function getDueTime(): ?DateTimeImmutable
    {
        if ($this->dueTime == null)
            return null;
        return new DateTimeImmutable($this->dueTime->format(DateTimeImmutable::ATOM));
    }

    /**
     * @param DateTimeImmutable|null $dueTime
     */
    public function setDueTime(?DateTimeImmutable $dueTime): void
    {
        $this->dueTime = $dueTime;
    }

    /**
     * @Field()
     *
     * @return ShoppingListItem[]|null
     */
    public function getShoppingListItems(): ?iterable
    {
        $result = [];

        /** @var ShoppingListItem $item */
        foreach ($this->shoppingListItems as $item) {
//            if ($item->getState() != EntityStateEnum::DELETED) {
                $result[] = $item;
//            }
        }

        return $result;
    }

    /**
     * @param ShoppingListItem[]|null $shoppingListItems
     * @param EntityManager $em
     * @throws ORMException
     */
    public function setShoppingListItems(?array $shoppingListItems, EntityManager $em): void
    {
        if (is_array($this->shoppingListItems) || is_object($this->shoppingListItems))
            foreach ($this->shoppingListItems as $shoppingListItem) {
                $em->remove($shoppingListItem);
                unset($shoppingListItem);
            }
        if (is_array($shoppingListItems) || is_object($shoppingListItems))
            foreach ($shoppingListItems as $shoppingListItem) {
                $shoppingListItem->setShoppingList($this);
                $this->shoppingListItems[] = $shoppingListItem;
            }
        $em->flush();
    }

    /**
     * @Field()
     *
     * @return User|null
     */
    public function getAssignee(): ?User
    {
        return $this->assignee;
    }

    /**
     * @param User|null $assignee
     */
    public function setAssignee(?User $assignee): void
    {
        $this->assignee = $assignee;
    }

    /**
     * @Field()
     *
     * @return EntityStateEnum
     */
    public function getState(): EntityStateEnum
    {
        return $this->state;
    }

    /**
     * @param EntityStateEnum $state
     */
    public function setState(EntityStateEnum $state): void
    {
        $this->state = $state;
    }

    /**
     * @Field()
     *
     * @return User|null
     */
    public function getCreator(): ?User
    {
        return $this->creator;
    }

    /**
     * @param User|null $creator
     */
    public function setCreator(?User $creator): void
    {
        $this->creator = $creator;
    }

}