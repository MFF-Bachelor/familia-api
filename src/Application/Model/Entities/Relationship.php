<?php


namespace App\Application\Model\Entities;

use App\Application\Model\Enum\GenderEnum;
use App\Application\Model\Enum\RelationshipSideEnum;
use App\Application\Model\Enum\RelationshipStateEnum;
use Doctrine\ORM\Mapping as ORM;
use App\Application\Model\Enum\RelationshipEnum;
use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;


/** GraphQLite annotations:
 * @Type(name="Relationship")
 *
 * Relationship entity
 *
 * ORM annotations:
 * @ORM\Table(name="relationship")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Relationship
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="relationshipsFrom", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_from_id", referencedColumnName="id")
     * })
     */
    private $userFrom;

    /**
     * @var User
     *
     * null if it is oneWayRelationship
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="relationshipsTo", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_to_id", referencedColumnName="id")
     * })
     */
    private $userTo;

    /**
     * @var RelationshipEnum
     *
     * @ORM\Column(name="role", type="relationship_enum", nullable=false)
     */
    private $role;

    /**
     * @var RelationshipSideEnum|null
     *
     * @ORM\Column(name="side", type="relationship_side_enum", nullable=true)
     */
    private $side;

    /**
     * @var RelationshipStateEnum
     *
     * @ORM\Column(name="state", type="relationship_state_enum", nullable=false)
     */
    private $state;

    /**
     * @var InactiveUser|null
     *
     * @OneToOne(targetEntity="InactiveUser")
     * @JoinColumn(name="inactive_user_to_id", referencedColumnName="id", nullable=true)
     */
    private $inactiveUserTo;

    /**
     * @var GenderEnum|null
     *
     * @ORM\Column(name="user_to_gender", type="gender_enum", nullable=true)
     */
    private $userToGender;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bio", type="text", nullable=true)
     */
    private $bio;

    /**
     * @param User $userFrom
     * @param User|null $userTo
     * @param InactiveUser|null $inactiveUserTo
     * @param GenderEnum $userToGender
     * @param string|null $bio
     * @param RelationshipEnum $role
     * @param RelationshipStateEnum $state
     * @param RelationshipSideEnum|null $side
     * @return Relationship
     */
    public static function create(
        User $userFrom,
        ?User $userTo,
        ?InactiveUser $inactiveUserTo,
        GenderEnum  $userToGender,
        ?string $bio,
        RelationshipEnum $role,
        RelationshipStateEnum $state,
        ?RelationshipSideEnum $side
    ) : Relationship {
        $instance = new self();

        $instance->userFrom = $userFrom;
        $instance->userTo = $userTo;
        $instance->role = $role;
        $instance->state = $state;
        $instance->side = $side;
        $instance->bio = $bio;
        $instance->userToGender = $userToGender;
        $instance->inactiveUserTo = $inactiveUserTo;

        return $instance;
    }

    /**
     * @Field()
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @Field()
     *
     * @return User
     */
    public function getUserFrom(): User
    {
        return $this->userFrom;
    }

    /**
     * @param User $userFrom
     */
    public function setUserFrom(User $userFrom): void
    {
        $this->userFrom = $userFrom;
    }

    /**
     * @Field()
     *
     * @return User|null
     */
    public function getUserTo(): ?User
    {
        return $this->userTo;
    }

    /**
     * @param User|null $userTo
     */
    public function setUserTo(?User $userTo): void
    {
        $this->userTo = $userTo;
    }

    /**
     * @Field()
     *
     * @return RelationshipEnum
     */
    public function getRole(): RelationshipEnum
    {
        return $this->role;
    }

    /**
     * @param RelationshipEnum $role
     */
    public function setRole(RelationshipEnum $role): void
    {
        $this->role = $role;
    }

    /**
     * @Field()
     *
     * @return RelationshipStateEnum
     */
    public function getState(): RelationshipStateEnum
    {
        return $this->state;
    }

    /**
     * @param RelationshipStateEnum $state
     */
    public function setState(RelationshipStateEnum $state): void
    {
        $this->state = $state;
    }

    /**
     * @Field()
     *
     * @return RelationshipSideEnum|null
     */
    public function getSide(): ?RelationshipSideEnum
    {
        return $this->side;
    }

    /**
     * @param RelationshipSideEnum|null $side
     */
    public function setSide(?RelationshipSideEnum $side): void
    {
        $this->side = $side;
    }

    /**
     * @Field()
     *
     * @return InactiveUser|null
     */
    public function getInactiveUserTo(): ?InactiveUser
    {
        return $this->inactiveUserTo;
    }

    /**
     * @param InactiveUser|null $inactiveUserTo
     */
    public function setInactiveUserTo(?InactiveUser $inactiveUserTo): void
    {
        $this->inactiveUserTo = $inactiveUserTo;
    }

    /**
     * @Field()
     *
     * @return GenderEnum|null
     */
    public function getUserToGender(): ?GenderEnum
    {
        return $this->userToGender;
    }

    /**
     * @param GenderEnum|null $userToGender
     */
    public function setUserToGender(?GenderEnum $userToGender): void
    {
        $this->userToGender = $userToGender;
    }

    /**
     * @Field()
     *
     * @return string|null
     */
    public function getBio(): ?string
    {
        return $this->bio;
    }

    /**
     * @param string|null $bio
     */
    public function setBio(?string $bio): void
    {
        $this->bio = $bio;
    }
}