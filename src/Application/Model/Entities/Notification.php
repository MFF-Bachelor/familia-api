<?php


namespace App\Application\Model\Entities;

use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\NotificationTypeEnum;
use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type(name="Notification")
 *
 * Notification entity
 *
 * ORM annotations:
 * @ORM\Table(name="notification")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Notification
{
    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", nullable=false)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string", nullable=false)
     */
    private $text;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    private $name;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="notifications", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var User|null
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="notificationsFrom", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_from_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $userFrom;

    /**
     * @var NotificationTypeEnum|null
     *
     * @ORM\Column(name="type", type="notification_type_enum", nullable=true)
     */
    private $type;

    /**
     * @var EntityStateEnum|null
     *
     * @ORM\Column(name="state", type="entity_state_enum", nullable=true)
     */
    private $state;

    /** Sent notifications
     *
     * @var SentNotification[]|null
     *
     * @ORM\OneToMany(targetEntity="SentNotification", mappedBy="notification", cascade={"persist", "remove"})
     */
    private $sentNotifications;

    /**
     * Notification constructor.
     * @param User $user
     * @param User|null $userFrom
     * @param string $title
     * @param string $text
     * @param string|null $type
     * @param string $state
     * @param string|null $name
     * @return Notification
     */
    public static function create(
        User $user,
        ?User $userFrom,
        string $title,
        string $text,
        string $type,
        string $state,
        ?string $name
    ) : Notification {
        $instance = new self();

        $instance->user = $user;
        $instance->userFrom = $userFrom;
        $instance->title = $title;
        $instance->text = $text;
        $instance->type = $type;
        $instance->state = $state;
        $instance->name = $name;

        return $instance;
    }

    /**
     * @Field()
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @Field()
     *
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @Field()
     *
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    /**
     * @Field()
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @Field()
     *
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @Field()
     *
     * @return User|null
     */
    public function getUserFrom(): ?User
    {
        return $this->userFrom;
    }

    /**
     * @param User|null $userFrom
     */
    public function setUserFrom(?User $userFrom): void
    {
        $this->userFrom = $userFrom;
    }

    /**
     * @Field()
     *
     * @return NotificationTypeEnum|null
     */
    public function getType(): ?NotificationTypeEnum
    {
        return $this->type;
    }

    /**
     * @param NotificationTypeEnum|null $type
     */
    public function setType(?NotificationTypeEnum $type): void
    {
        $this->type = $type;
    }

    /**
     * @Field()
     *
     * @return EntityStateEnum|null
     */
    public function getState(): ?EntityStateEnum
    {
        return $this->state;
    }

    /**
     * @param EntityStateEnum|null $state
     */
    public function setState(?EntityStateEnum $state): void
    {
        $this->state = $state;
    }

    /**
     * @Field()
     *
     * @return SentNotification[]|null
     */
    public function getSentNotifications(): ?iterable
    {
        return $this->sentNotifications;
    }

    /**
     * @param SentNotification[]|null $sentNotifications
     */
    public function setSentNotifications(?array $sentNotifications): void
    {
        $this->sentNotifications = $sentNotifications;
    }


}