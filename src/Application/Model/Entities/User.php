<?php

namespace App\Application\Model\Entities;

use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\GenderEnum;
use App\Application\Model\Enum\TaskStateEnum;
use App\Application\Model\Timestampable;
use DateTimeImmutable;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\ORMException;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations
 * @Type(name="User")
 *
 * User entity
 *
 * @ORM\Table(name="user", uniqueConstraints={@ORM\UniqueConstraint(name="uid", columns={"uid"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class User
{

    #region private fields

    use Timestampable;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="uid", type="string", length=255, unique=true, nullable=false)
     */
    private $uid;

    /**
     * @var EntityStateEnum
     *
     * @ORM\Column(name="state", type="entity_state_enum", nullable=false)
     */
    private $state;

    /**
     * @var string|null
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @var UserEmail[]|null
     *
     * @ORM\OneToMany(targetEntity="UserEmail", mappedBy="user", cascade={"persist", "remove"})
     */
    private $emails;

    /**
     * @var UserPhoneNumber[]|null
     *
     * @ORM\OneToMany(targetEntity="UserPhoneNumber", mappedBy="user", cascade={"persist", "remove"})
     */
    private $phoneNumbers;

    /**
     * @var DateTimeImmutable|null
     * @ORM\Column(name="birth_date", type="datetime", nullable=true)
     */
    protected $birthDate;

    /**
     * @var GenderEnum|null
     *
     * @ORM\Column(name="gender", type="gender_enum", nullable=true)
     */
    private $gender;

    /**
     * @var string|null
     *
     * @ORM\Column(name="street", type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descriptive_number", type="string", nullable=true)
     */
    private $descriptiveNumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="routing_number", type="string", nullable=true)
     */
    private $routingNumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="postcode", type="string", length=5, nullable=true)
     */
    private $postcode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string|null
     *
     * @ORM\Column(name="country", type="string", nullable=true)
     */
    private $country;

    /**
     * @var Relationship[]|null
     *
     * @ORM\OneToMany(targetEntity="Relationship", mappedBy="userFrom", cascade={"persist", "remove"})
     */
    private $relationshipsFrom;

    /**
     * @var Relationship[]|null
     *
     * @ORM\OneToMany(targetEntity="Relationship", mappedBy="userTo", cascade={"persist", "remove"})
     */
    private $relationshipsTo;

    /**
     * @var Task[]|null
     *
     * @ORM\OneToMany(targetEntity="Task", mappedBy="creator", cascade={"persist", "remove"})
     */
    private $createdTasks;

    /**
     * @var Task[]|null
     *
     * @ORM\OneToMany(targetEntity="Task", mappedBy="assignee", cascade={"persist", "remove"})
     */
    private $assigneeTasks;

    /**
     * @var Notification[]|null
     *
     * @ORM\OneToMany(targetEntity="Notification", mappedBy="user", cascade={"persist", "remove"})
     */
    private $notifications;

    /**
     * @var Notification[]|null
     *
     * @ORM\OneToMany(targetEntity="Notification", mappedBy="userFrom", cascade={"persist", "remove"})
     */
    private $notificationsFrom;

    /** FCM tokens belong to user
     *
     * @var FcmToken[]|null
     *
     * @ORM\OneToMany(targetEntity="FcmToken", mappedBy="user", cascade={"persist", "remove"})
     */
    private $fcmTokens;

    /**
     * @var UserRecipe[]|null
     *
     * @ORM\OneToMany(targetEntity="UserRecipe", mappedBy="user", cascade={"persist", "remove"})
     */
    private $recipes;

    /**
     * @var UserContact[]|null
     *
     * @ORM\OneToMany(targetEntity="UserContact", mappedBy="user", cascade={"persist", "remove"})
     */
    private $contacts;

    /**
     * @var ShoppingList[]|null
     *
     * @ORM\OneToMany(targetEntity="ShoppingList", mappedBy="creator", cascade={"persist", "remove"})
     */
    private $createdShoppingLists;

    /**
     * @var ShoppingList[]|null
     *
     * @ORM\OneToMany(targetEntity="ShoppingList", mappedBy="assignee", cascade={"persist", "remove"})
     */
    private $assigneeShoppingLists;
    #endregion

    #region constructor //provided by create function

    /**
     * User constructor.
     * @param string $uid
     * @param EntityStateEnum $state
     * @param string|null $firstName
     * @param string|null $lastName
     * @param array|null $emails
     * @param array|null $phoneNumbers
     * @param DateTimeImmutable|null $birthDate
     * @param GenderEnum|null $gender
     * @param string|null $street
     * @param string|null $descriptiveNumber
     * @param string|null $routingNumber
     * @param string|null $postcode
     * @param string|null $city
     * @param string|null $country
     * @param EntityManager $em
     * @return User
     * @throws ORMException
     */
    public static function create(
        string $uid,
        EntityStateEnum $state,
        string $firstName = null,
        string $lastName = null,
        ?array $emails,
        ?array  $phoneNumbers,
        DateTimeImmutable $birthDate = null,
        GenderEnum $gender = null,
        string $street = null,
        ?string $descriptiveNumber,
        ?string $routingNumber,
        string $postcode = null,
        string $city = null,
        string $country = null,
        EntityManager $em
    ) : User
    {
        $instance = new self();

        $instance->uid = $uid;
        $instance->state = $state;
        $instance->firstName = $firstName;
        $instance->lastName = $lastName;
        $instance->setEmails($emails, $em);
        $instance->setPhoneNumbers($phoneNumbers, $em);
        $instance->birthDate = $birthDate;
        $instance->gender = $gender;
        $instance->street = $street;
        $instance->descriptiveNumber = $descriptiveNumber;
        $instance->routingNumber = $routingNumber;
        $instance->postcode = $postcode;
        $instance->city = $city;
        $instance->country = $country;

        return $instance;
    }

    #endregion

    #region getters and setters

    /**
     * @Field()
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @Field()
     *
     * @return string
     */
    public function getUid(): string
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     */
    public function setUid(string $uid): void
    {
        $this->uid = $uid;
    }

    /**
     * @Field()
     *
     * @return EntityStateEnum|null
     */
    public function getState(): ?EntityStateEnum
    {
        return $this->state;
    }

    /**
     * @param EntityStateEnum $state
     */
    public function setState(EntityStateEnum $state): void
    {
        $this->state = $state;
    }

    /**
     * @Field()
     *
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName(string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @Field()
     *
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName(string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @Field()
     *
     * @return UserEmail[]|null
     */
    public function getEmails(): ?iterable
    {
        return $this->emails;
    }

    /**
     * @param string[]|null $emails
     * @param EntityManager $em
     * @throws ORMException
     */
    public function setEmails(?array $emails, EntityManager $em): void
    {
        if (is_array($this->emails) || is_object($this->emails)) {
            foreach ($this->emails as $oldEmail) {
                $foundItem = array_search($oldEmail->getEmail(), $emails);
                if (!is_bool($foundItem)) {     //if the new email is in the list of old emails
                    unset($emails[$foundItem]);
                    continue;
                }
                $em->remove($oldEmail);
                unset($oldEmail);
            }
        }
        if (is_array($emails) || is_object($emails)) {
            foreach ($emails as $newEmail) {
                if (strlen(trim($newEmail)) != 0) {
                    $newShopsEmail = UserEmail::create($this, $newEmail);
                    $this->emails[] = $newShopsEmail;
                }
            }
        }
    }

    /**
     * @Field()
     *
     * @return UserPhoneNumber[]|null
     */
    public function getPhoneNumbers(): ?iterable
    {
        return $this->phoneNumbers;
    }

    /**
     * @param string[]|null $phoneNumbers
     * @param EntityManager $em
     * @throws ORMException
     */
    public function setPhoneNumbers(?array $phoneNumbers, EntityManager $em): void
    {
        if (is_array($this->phoneNumbers) || is_object($this->phoneNumbers)) {
            foreach ($this->phoneNumbers as $oldPhoneNumber) {
                $foundItem = array_search($oldPhoneNumber->getPhoneNumber(), $phoneNumbers);
                if (!is_bool($foundItem)) {     //if the new phone number is in the list of old phone numbers
                    unset($phoneNumbers[$foundItem]);
                    continue;
                }
                $em->remove($oldPhoneNumber);
                unset($oldPhoneNumber);
            }
        }
        if (is_array($phoneNumbers) || is_object($phoneNumbers)) {
            foreach ($phoneNumbers as $newPhoneNumber) {
                if (strlen(trim($newPhoneNumber)) != 0) {
                    $newUserPhoneNumber = UserPhoneNumber::create($this, $newPhoneNumber);
                    $this->phoneNumbers[] = $newUserPhoneNumber;
                }
            }
        }
    }

    /**
     * @Field()
     *
     * @return DateTimeImmutable|null
     * @throws Exception
     */
    public function getBirthDate(): ?DateTimeImmutable
    {
        if (is_null($this->birthDate))
            return null;
        return new DateTimeImmutable($this->birthDate->format(DateTimeImmutable::ATOM));
    }

    /**
     * @param DateTimeImmutable|null $birthDate
     */
    public function setBirthDate(?DateTimeImmutable $birthDate): void
    {
        $this->birthDate = $birthDate;
    }

    /**
     *  @Field()
     *
     * @return GenderEnum|null
     */
    public function getGender(): ?GenderEnum
    {
        return $this->gender;
    }

    /**
     * @param GenderEnum $gender
     */
    public function setGender(GenderEnum $gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @Field()
     *
     * @return string|null
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * @param string $street
     */
    public function setStreet(string $street): void
    {
        $this->street = $street;
    }

    /**
     * @Field()
     *
     * @return string|null
     */
    public function getDescriptiveNumber(): ?string
    {
        return $this->descriptiveNumber;
    }

    /**
     * @param string|null $descriptiveNumber
     */
    public function setDescriptiveNumber(?string $descriptiveNumber): void
    {
        $this->descriptiveNumber = $descriptiveNumber;
    }

    /**
     * @Field()
     *
     * @return string|null
     */
    public function getRoutingNumber(): ?string
    {
        return $this->routingNumber;
    }

    /**
     * @param string|null $routingNumber
     */
    public function setRoutingNumber(?string $routingNumber): void
    {
        $this->routingNumber = $routingNumber;
    }

    /**
     * @Field()
     *
     * @return string|null
     */
    public function getPostcode(): ?string
    {
        return $this->postcode;
    }

    /**
     * @param string $postcode
     */
    public function setPostcode(string $postcode): void
    {
        $this->postcode = $postcode;
    }

    /**
     * @Field()
     *
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string $city
     */
    public function setCity(string $city): void
    {
        $this->city = $city;
    }

    /**
     * @Field()
     *
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string $country
     */
    public function setCountry(string $country): void
    {
        $this->country = $country;
    }

    #endregion

    /**
     * @Field()
     *
     * @return Relationship[]|null
     */
    public function getRelationshipsFrom(): ?iterable
    {
        return $this->relationshipsFrom;
    }

    /**
     * @param Relationship[]|null $relationshipsFrom
     */
    public function setRelationshipsFrom(?array $relationshipsFrom): void
    {
        $this->relationshipsFrom = $relationshipsFrom;
    }

    /**
     * @Field()
     *
     * @return Relationship[]|null
     */
    public function getRelationshipsTo(): ?iterable
    {
        return $this->relationshipsTo;
    }

    /**
     * @param Relationship[]|null $relationshipsTo
     */
    public function setRelationshipsTo(?array $relationshipsTo): void
    {
        $this->relationshipsTo = $relationshipsTo;
    }

    /**
     * @Field()
     *
     * @return Notification[]|null
     */
    public function getNotifications(): ?iterable
    {
        $result = [];
        if (!is_null($this->notifications) && (is_array($this->notifications) || is_object($this->notifications))) {
            foreach ($this->notifications as $notification) {
                if ($notification->getState() != EntityStateEnum::DELETED) {
                    $result[] = $notification;
                }
            }
        }
        
        return $result;
    }

    /**
     * @param Notification[]|null $notifications
     */
    public function setNotifications(?array $notifications): void
    {
        $this->notifications = $notifications;
    }

    /**
     * @Field()
     *
     * @return Notification[]|null
     */
    public function getNotificationsFrom(): ?iterable
    {
        return $this->notificationsFrom;
    }

    /**
     * @param Notification[]|null $notificationsFrom
     */
    public function setNotificationsFrom(?array $notificationsFrom): void
    {
        $this->notificationsFrom = $notificationsFrom;
    }

    /**
     * @Field()
     *
     *
     * @return Task[]|null
     */
    public function getOtherCreatedTasks(): ?iterable
    {
        $result = [];

        if (is_array($this->createdTasks) || is_object($this->createdTasks)) {
            foreach ($this->createdTasks->toArray() as $task) {
                if ($task->getCreator() !== $task->getAssignee() && $task->getState() != TaskStateEnum::DELETED)
                    array_push($result, $task);
            }
        }

        return $result;
    }

    /**
     * @Field
     *
     * @return UserRecipe[]|null
     */
    public function getRecipes(): ?iterable
    {
        $result = [];

        foreach ($this->recipes as $recipe) {
            if ($recipe->getState() != EntityStateEnum::DELETED) {
                $result[] = $recipe;
            }
        }

        return $result;
    }

    /**
     * @param UserRecipe[]|null $recipes
     */
    public function setRecipes(?array $recipes): void
    {
        $this->recipes = $recipes;
    }

    /**
     * @Field()
     *
     * @return UserContact[]|null
     */
    public function getContacts(): ?iterable
    {

        $result = [];

        foreach ($this->contacts as $contact) {
            if ($contact->getState() != EntityStateEnum::DELETED) {
                $result[] = $contact;
            }
        }

        return $result;
    }

    /**
     * @param UserContact[]|null $contacts
     */
    public function setContacts(?array $contacts): void
    {
        $this->contacts = $contacts;
    }

    /**
     * @Field()
     *
     * @return ShoppingList[]|null
     */
    public function getCreatedShoppingLists(): ?iterable
    {
        $result = [];

        if (is_array($this->createdShoppingLists) || is_object($this->createdShoppingLists)) {
            foreach ($this->createdShoppingLists as $shoppingList) {
                if ($shoppingList->getState() != EntityStateEnum::DELETED) {
                    $result[] = $shoppingList;
                }
            }
        }

        return $result;

    }

    /**
     * @param ShoppingList[]|null $createdShoppingLists
     */
    public function setCreatedShoppingLists(?array $createdShoppingLists): void
    {
        $this->createdShoppingLists = $createdShoppingLists;
    }

    /**
     * @Field()
     *
     * @return ShoppingList[]|null
     */
    public function getAssigneeShoppingLists(): ?iterable
    {
        $result = [];

        if (is_array($this->assigneeShoppingLists) || is_object($this->assigneeShoppingLists)) {
            foreach ($this->assigneeShoppingLists as $shoppingList) {
                if ($shoppingList->getState() != EntityStateEnum::DELETED) {
                    $result[] = $shoppingList;
                }
            }
        }

        return $result;

    }

    /**
     * @param ShoppingList[]|null $assigneeShoppingLists
     */
    public function setAssigneeShoppingLists(?array $assigneeShoppingLists): void
    {
        $this->assigneeShoppingLists = $assigneeShoppingLists;
    }

    /**
     * @param Task[]|null $createdTasks
     */
    private function setCreatedTasks(?array $createdTasks): void
    {
        $this->createdTasks = $createdTasks;
    }

    /**
     * @Field()
     *
     * @return Task[]
     */
    public function getAssignedTasks(): iterable
    {

        $result = [];

        if (is_array($this->assigneeTasks) || is_object($this->assigneeTasks)) {
            foreach ($this->assigneeTasks->toArray() as $task) {
                if ($task->getState() != TaskStateEnum::DELETED)
                    array_push($result, $task);
            }
        }

        return $result;

//        if (is_null($this->assigneeTasks))
//            return [];
//        return $this->assigneeTasks;
    }

    /**
     * @param Task[]|null $assigneeTasks
     */
    public function setAssignedTasks(?array $assigneeTasks): void
    {
        $this->assigneeTasks = $assigneeTasks;
    }
}
