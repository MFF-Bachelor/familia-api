<?php

namespace App\Application\Model\Entities;

use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\RecipeIngredientUnitEnum;
use App\Application\Model\Enum\RecipePreparationTimeUnitEnum;
use App\Application\Model\Enum\TaskStateEnum;
use App\Application\Model\Enum\UserRecipeTypeEnum;
use App\Application\Model\Timestampable;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Boolean;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;


/** GraphQLite annotations:
 * @Type(name="UserRecipe")
 *
 * UserRecipe entity
 *
 * ORM annotations:
 * @ORM\Table(name="user_recipe")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class UserRecipe {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="recipes", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


    /**
     * @var Recipe
     *
     * @ORM\ManyToOne(targetEntity="Recipe", inversedBy="recipeUsers", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="recipe_id", referencedColumnName="id")
     * })
     */
    private $recipe;

    /**
     * @var bool
     *
     * @ORM\Column(name="favourite", type="boolean", nullable=false)
     */
    private $favourite;

    /**
     * @var int|null
     *
     * @ORM\Column(name="rating", type="integer", nullable=true)
     */
    private $rating;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="string", nullable=true)
     */
    private $comment;

    /**
     * @var UserRecipeTypeEnum
     *
     * @ORM\Column(name="type", type="user_recipe_type_enum", nullable=false)
     */
    private $type;

    /**
     * @var bool
     *
     * @ORM\Column(name="will_cook", type="boolean", nullable=false)
     */
    private $willCook;

    /**
     * @var EntityStateEnum
     *
     * @ORM\Column(name="state", type="entity_state_enum", nullable=false)
     */
    private $state; //mozno lepsie nazvat visible - hodnoty: ACTIVE, DELETED

    /**
     * @param User $user
     * @param Recipe $recipe
     * @param bool $favourite
     * @param int|null $rating
     * @param string|null $comment
     * @param UserRecipeTypeEnum $type
     * @param bool $willCook
     * @return UserRecipe
     */
    public static function create(
        User $user,
        Recipe $recipe,
        bool $favourite,
        int $rating = null,
        string $comment = null,
        UserRecipeTypeEnum $type,
        bool $willCook,
        EntityStateEnum $state
    ) : UserRecipe {
        $instance = new self();

        $instance->user = $user;
        $instance->recipe = $recipe;
        $instance->favourite = $favourite;
        $instance->rating = $rating;
        $instance->comment = $comment;
        $instance->type = $type;
        $instance->willCook = $willCook;
        $instance->state = $state;

        return $instance;
    }

    /**
     * @Field
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @Field
     *
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    /**
     * @Field
     *
     * @return Recipe
     */
    public function getRecipe(): Recipe
    {
        return $this->recipe;
    }

    /**
     * @param Recipe $recipe
     */
    public function setRecipe(Recipe $recipe): void
    {
        $this->recipe = $recipe;
    }

    /**
     * @Field
     *
     * @return bool
     */
    public function getFavourite(): bool
    {
        return $this->favourite;
    }

    /**
     * @param bool $favourite
     */
    public function setFavourite(bool $favourite): void
    {
        $this->favourite = $favourite;
    }

    /**
     * @Field
     *
     * @return int|null
     */
    public function getRating(): ?int
    {
        return $this->rating;
    }

    /**
     * @param int|null $rating
     */
    public function setRating(?int $rating): void
    {
        $this->rating = $rating;
    }

    /**
     * @Field
     *
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string|null $comment
     */
    public function setComment(?string $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @Field
     *
     * @return UserRecipeTypeEnum
     */
    public function getType(): UserRecipeTypeEnum
    {
        return $this->type;
    }

    /**
     * @param UserRecipeTypeEnum $type
     */
    public function setType(UserRecipeTypeEnum $type): void
    {
        $this->type = $type;
    }

    /**
     * @Field
     *
     * @return bool
     */
    public function getWillCook(): bool
    {
        return $this->willCook;
    }

    /**
     * @param bool $willCook
     */
    public function setWillCook(bool $willCook): void
    {
        $this->willCook = $willCook;
    }

    /**
     * @Field
     *
     * @return EntityStateEnum
     */
    public function getState(): EntityStateEnum
    {
        return $this->state;
    }

    /**
     * @param EntityStateEnum $state
     */
    public function setState(EntityStateEnum $state): void
    {
        $this->state = $state;
    }
}