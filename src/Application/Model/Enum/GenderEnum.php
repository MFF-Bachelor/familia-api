<?php


namespace App\Application\Model\Enum;


use MyCLabs\Enum\Enum;

class GenderEnum extends Enum
{
    public const MALE = 'MALE';
    public const FEMALE = 'FEMALE';
}