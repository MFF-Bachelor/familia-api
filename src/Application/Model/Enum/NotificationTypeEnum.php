<?php


namespace App\Application\Model\Enum;


use App\Application\Model\Entities\User;
use MyCLabs\Enum\Enum;

class NotificationTypeEnum extends Enum
{
    public const CREATE_RELATIONSHIP = 'CREATE_RELATIONSHIP';
    public const CONFIRM_RELATIONSHIP = 'CONFIRM_RELATIONSHIP';
    public const REJECT_RELATIONSHIP = 'REJECT_RELATIONSHIP';
    public const DELETE_RELATIONSHIP = 'DELETE_RELATIONSHIP';
    public const DONE_TASK = 'DONE_TASK';
    public const ASSIGN_TASK = 'ASSIGN_TASK';
    public const SHARE_RECIPE = 'SHARE_RECIPE';
    public const ASSIGN_SHOPPING_LIST = 'ASSIGN_SHOPPING_LIST';

    public function getNotificationTitle() {
        switch ($this) {
            case self::CREATE_RELATIONSHIP:
                return "Žiadosť o pridanie do rodiny";
            case self::CONFIRM_RELATIONSHIP:
                return "Žiadosť o pridanie do rodiny bola prijatá";
            case self::REJECT_RELATIONSHIP:
                return "Žiadosť o pridanie do rodiny nebola prijatá";
            case self::DELETE_RELATIONSHIP:
                return "Vzťah bol odstránený";
            case self::DONE_TASK:
                return "Úloha bola dokončená";
            case self::ASSIGN_TASK:
                return "Máte novú úlohu";
            case self::SHARE_RECIPE:
                return "Máte nový recept";
            case self::ASSIGN_SHOPPING_LIST:
                return "Máte nový nákupný zoznam";
            default:
                return "Máte novú notifikáciu.";
        }
    }

    public function getNotificationText(?User $userFrom, ?User $userTo) {
        switch ($this) {
            case self::CREATE_RELATIONSHIP:
                return "Používateľ " . $userFrom->getFirstName() . " " . $userFrom->getLastName()  . " vás žiada o pridanie do jeho rodiny.";
            case self::CONFIRM_RELATIONSHIP:
                return "Používateľ " . $userFrom->getFirstName() . " " . $userFrom->getLastName()  . " prijal vašu žiadosť o pridanie do rodiny.";
            case self::REJECT_RELATIONSHIP:
                return "Používateľ " . $userFrom->getFirstName() . " " . $userFrom->getLastName()  . " odmietol vašu žiadosť o pridanie do rodiny.";
            case self::DELETE_RELATIONSHIP:
                return "Používateľ " . $userFrom->getFirstName() . " " . $userFrom->getLastName()  . " vás odstránil z jeho rodiny.";
            case self::DONE_TASK:
                return "Používateľ " . $userFrom->getFirstName() . " " . $userFrom->getLastName()  . " dokončil/a úlohu od vás.";
            case self::ASSIGN_TASK:
                return "Používateľ " . $userFrom->getFirstName() . " " . $userFrom->getLastName()  . " vám priradil novú úlohu.";
            case self::SHARE_RECIPE:
                return "Používateľ " . $userFrom->getFirstName() . " " . $userFrom->getLastName()  . " s vami zdieľal recept.";
            case self::ASSIGN_SHOPPING_LIST:
                return "Používateľ " . $userFrom->getFirstName() . " " . $userFrom->getLastName()  . " vám priradil nový nákupný zoznam.";
            default:
                return "";
        }
    }
}