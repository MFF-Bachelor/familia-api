<?php


namespace App\Application\Model\Enum;


use MyCLabs\Enum\Enum;

class UserContactTypeEnum extends Enum
{
    public const PHONE_NUMBER = 'PHONE_NUMBER';
    public const EMAIL = 'EMAIL';
}