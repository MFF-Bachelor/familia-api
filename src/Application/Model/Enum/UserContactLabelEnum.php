<?php


namespace App\Application\Model\Enum;


use MyCLabs\Enum\Enum;

class UserContactLabelEnum extends Enum
{
    public const HOME = 'HOME';
    public const WORK = 'WORK';
    public const MAIN = 'MAIN';
    public const FAX = 'FAX';
}