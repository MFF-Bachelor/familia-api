<?php


namespace App\Application\Model\Enum;


class RecipePreparationTimeUnitEnum extends EntityStateEnum
{
    public const SECOND = 'SECOND';
    public const MINUTE = 'MINUTE';
    public const HOUR = 'HOUR';
    public const DAY = 'DAY';
}