<?php


namespace App\Application\Model\Enum;


class TaskStateEnum extends EntityStateEnum
{
    public const DONE = 'DONE';
}