<?php


namespace App\Application\Model\Enum;


class UserRecipeTypeEnum extends EntityStateEnum
{
    public const CREATOR = 'CREATOR';
    public const VIEWER = 'VIEWER';
}