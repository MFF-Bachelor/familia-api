<?php


namespace App\Application\Model\Enum;


class RelationshipStateEnum extends EntityStateEnum
{
    public const TO_CONFIRM = 'TO_CONFIRM';
    public const NOT_CONFIRMED = 'NOT_CONFIRMED';
}