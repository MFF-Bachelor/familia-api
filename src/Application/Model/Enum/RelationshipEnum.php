<?php


namespace App\Application\Model\Enum;


use MyCLabs\Enum\Enum;

class RelationshipEnum extends Enum
{
    public const PARENT = 'PARENT';
    public const CHILD = 'CHILD';
    public const PARENT_IN_LAW = 'PARENT_IN_LAW'; // svokor svokra
    public const GRAND_PARENT = 'GRAND_PARENT';
    public const GRAND_GRAND_PARENT = 'GRAND_GRAND_PARENT';
    public const CHILD_IN_LAW = 'CHILD_IN_LAW'; //zat nevesta
    public const UNMARRIED_PARTNER = 'UNMARRIED_PARTNER';
    public const FRIEND = 'FRIEND';
    public const PARTNER = 'PARTNER';
    public const COUSIN = 'COUSIN';
    public const GRAND_CHILD = 'GRAND_CHILD';
    public const GRAND_GRAND_CHILD = 'GRAND_GRAND_CHILD';
    public const SIBLING = 'SIBLING';
    public const STEP_SIBLING = 'STEP_SIBLING';
    public const SIBLING_IN_LAW = 'SIBLING_IN_LAW'; //svagor svagrina
    public const NEPHEW_NIECE = 'NEPHEW_NIECE'; // neter synovec
    public const AUNT_UNCLE = 'AUNT_UNCLE'; // stryko teta


    public function getLimit() {
        switch ($this) {
            case self::PARTNER:
            case self::PARENT:
                return 1;
            case self::GRAND_PARENT:
                return 2;
            default:
                return -1;
        }
    }
}