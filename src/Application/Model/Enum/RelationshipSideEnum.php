<?php


namespace App\Application\Model\Enum;


use MyCLabs\Enum\Enum;

class RelationshipSideEnum extends Enum
{
    public const MATERNAL = 'MATERNAL';
    public const  PATERNAL = 'PATERNAL';
}