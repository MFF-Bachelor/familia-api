<?php


namespace App\Application\Model\Enum;


class RecipeIngredientUnitEnum extends EntityStateEnum
{
    public const L = 'l';
    public const ML  = 'ml';
    public const G = 'g';
    public const KG = 'kg';
    public const KS = 'ks';
    public const PINCH = 'štipka';
    public const TABLE_SPOON = 'p.l.';
    public const TEA_SPOON = 'č.l.';
    public const DCL = 'dcl';
    public const DKG = 'dKg';
}