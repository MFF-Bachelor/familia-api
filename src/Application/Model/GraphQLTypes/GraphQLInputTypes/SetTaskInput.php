<?php

namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;

use App\Application\Model\Enum\TaskStateEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateTaskInput;
use DateTimeImmutable;

class SetTaskInput
{
    /**
     * @var int
     */
    private $taskId;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string|null
     */
    private $text;

    /**
     * @var DateTimeImmutable|null
     */
    protected $dueTime;

    /**
     * @var string
     */
    private $assigneeUid;

    /**
     * @var int
     */
    private $priority;

    /**
     * @var TaskStateEnum
     */
    private $state;

    /**
     * @var boolean
     */
    private $canAssigneeEdit;

    /**
     * @param string $taskId
     * @param string $title
     * @param string|null $text
     * @param DateTimeImmutable|null $dueTime
     * @param int $assigneeUid
     * @param int $priority
     * @param TaskStateEnum $state
     * @param bool $canAssigneeEdit
     * @return SetTaskInput
     */
    public static function create(
        string $taskId,
        string $title,
        ?string $text,
        ?DateTimeImmutable $dueTime,
        string $assigneeUid,
        int $priority,
        TaskStateEnum $state,
        bool $canAssigneeEdit
    ) : SetTaskInput
    {
        $instance = new self();

        $instance->taskId = $taskId;
        $instance->title = $title;
        $instance->text = $text;
        $instance->dueTime = $dueTime;
        $instance->assigneeUid = $assigneeUid;
        $instance->state = $state;
        $instance->priority = $priority;
        $instance->canAssigneeEdit = $canAssigneeEdit;

        return $instance;
    }

    /**
     * @return int
     */
    public function getTaskId(): int
    {
        return $this->taskId;
    }

    /**
     * @param int $taskId
     */
    public function setTaskId(int $taskId): void
    {
        $this->taskId = $taskId;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getDueTime(): ?DateTimeImmutable
    {
        return $this->dueTime;
    }

    /**
     * @param DateTimeImmutable|null $dueTime
     */
    public function setDueTime(?DateTimeImmutable $dueTime): void
    {
        $this->dueTime = $dueTime;
    }

    /**
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     */
    public function setPriority(int $priority): void
    {
        $this->priority = $priority;
    }

    /**
     * @return TaskStateEnum
     */
    public function getState(): TaskStateEnum
    {
        return $this->state;
    }

    /**
     * @param TaskStateEnum $state
     */
    public function setState(TaskStateEnum $state): void
    {
        $this->state = $state;
    }

    /**
     * @return bool
     */
    public function isCanAssigneeEdit(): bool
    {
        return $this->canAssigneeEdit;
    }

    /**
     * @param bool $canAssigneeEdit
     */
    public function setCanAssigneeEdit(bool $canAssigneeEdit): void
    {
        $this->canAssigneeEdit = $canAssigneeEdit;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     */
    public function setText(?string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getAssigneeUid(): string
    {
        return $this->assigneeUid;
    }

    /**
     * @param string $assigneeUid
     */
    public function setAssigneeUid(string $assigneeUid): void
    {
        $this->assigneeUid = $assigneeUid;
    }
}