<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;

use App\Application\Model\Enum\GenderEnum;
use App\Application\Model\Enum\RelationshipEnum;
use App\Application\Model\Enum\RelationshipSideEnum;
use DateTimeImmutable;

class CreateOneWayRelationshipInput
{

    /**
     * @var string|null
     */
    private $firstName;

    /**
     * @var string|null
     */
    private $lastName;

    /**
     * @var DateTimeImmutable|null
     */
    private $birthDate;

    /**
     * @var RelationshipEnum
     */
    private $role;

    /**
     * @var GenderEnum|null
     */
    private $gender;

    /**
     * @var RelationshipSideEnum|null
     */
    private $side;

    /**
     * @var string|null
     */
    private $bio;

    /**
     * @param string|null $firstName
     * @param string|null $lastName
     * @param DateTimeImmutable|null $birthDate
     * @param GenderEnum|null $gender
     * @param RelationshipEnum $role
     * @param RelationshipSideEnum|null $side
     * @param string|null $bio
     * @return CreateOneWayRelationshipInput
     */
    public static function create(
        ?string $firstName,
        ?string $lastName,
        ?DateTimeImmutable $birthDate,
        ?GenderEnum $gender,
        RelationshipEnum $role,
        ?RelationshipSideEnum $side,
        ?string $bio
    ) : CreateOneWayRelationshipInput
    {
        $instance = new self();

        $instance->firstName = $firstName;
        $instance->lastName = $lastName;
        $instance->birthDate = $birthDate;
        $instance->gender = $gender;
        $instance->role = $role;
        $instance->side = $side;
        $instance->bio = $bio;

        return $instance;
    }


    /**
     * @return RelationshipEnum
     */
    public function getRole(): RelationshipEnum
    {
        return $this->role;
    }

    /**
     * @param RelationshipEnum $role
     */
    public function setRole(RelationshipEnum $role): void
    {
        $this->role = $role;
    }

    /**
     * @return RelationshipSideEnum|null
     */
    public function getSide(): ?RelationshipSideEnum
    {
        return $this->side;
    }

    /**
     * @param RelationshipSideEnum|null $side
     */
    public function setSide(?RelationshipSideEnum $side): void
    {
        $this->side = $side;
    }

    /**
     * @return GenderEnum|null
     */
    public function getGender(): ?GenderEnum
    {
        return $this->gender;
    }

    /**
     * @param GenderEnum|null $gender
     */
    public function setGender(?GenderEnum $gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     */
    public function setFirstName(?string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     */
    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getBirthDate(): ?DateTimeImmutable
    {
        return $this->birthDate;
    }

    /**
     * @param DateTimeImmutable|null $birthDate
     */
    public function setBirthDate(?DateTimeImmutable $birthDate): void
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @return string|null
     */
    public function getBio(): ?string
    {
        return $this->bio;
    }

    /**
     * @param string|null $bio
     */
    public function setBio(?string $bio): void
    {
        $this->bio = $bio;
    }
}