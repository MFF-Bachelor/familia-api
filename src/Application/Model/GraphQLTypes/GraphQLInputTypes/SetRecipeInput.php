<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


use App\Application\Model\Entities\RecipeIngredient;
use App\Application\Model\Entities\RecipeInstruction;
use App\Application\Model\Enum\RecipePreparationTimeUnitEnum;

class SetRecipeInput
{
    /**
     * @var int
     */
    private $userRecipeId;

    /**
     * @var string
     */
    private $title;

    /**
     * @var int|null
     */
    private $servings;

    /**
     * @var int|null
     */
    private $caloriesPerServing;

    /**
     * @var string|null
     */
    private $source;

    /**
     * @var int|null
     */
    private $preparationTime;

    /**
     * @var RecipePreparationTimeUnitEnum|null
     */
    private $preparationTimeUnit;

    /**
     * @var int|null
     */
    private $difficulty;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var RecipeIngredient[]|null
     */
    private $recipeIngredients;

    /**
     * @var RecipeInstruction[]|null
     */
    private $recipeInstructions;

    /**
     * @var bool
     */
    private $willCook;

    /**
     * @var int[]|null
     */
    private $recipeCategories;

    /**
     * @var string|null
     */
    private $comment;

    /**
     * @var int|null
     */
    private $rating;

    /**
     * @var bool
     */
    private $favourite;

    /**
     * @param int $recipeId
     * @param string $title
     * @param int|null $servings
     * @param int|null $caloriesPerServing
     * @param string|null $source
     * @param int|null $preparationTime
     * @param RecipePreparationTimeUnitEnum|null $preparationTimeUnit
     * @param int|null $difficulty
     * @param string|null $description
     * @param RecipeIngredient[]|null $recipeIngredient
     * @param RecipeInstruction[]|null $recipeInstruction
     * @param bool $willCook
     * @param array|null $recipeCategories
     * @param string|null $comment
     * @param int|null $rating
     * @param bool $favourite
     * @return SetRecipeInput
     */
    public static function create(
        int $recipeId,
        string $title,
        ?int $servings,
        ?int $caloriesPerServing,
        ?string $source,
        ?int $preparationTime,
        ?RecipePreparationTimeUnitEnum $preparationTimeUnit,
        ?int $difficulty,
        ?string $description,
        ?array $recipeIngredient,
        ?array $recipeInstruction,
        bool $willCook,
        ?array $recipeCategories,
        ?string $comment,
        ?int $rating,
        bool $favourite
    ): SetRecipeInput
    {
        $instance = new self();

        $instance->userRecipeId = $recipeId;
        $instance->title = $title;
        $instance->servings = $servings;
        $instance->caloriesPerServing = $caloriesPerServing;
        $instance->source = $source;
        $instance->preparationTime = $preparationTime;
        $instance->preparationTimeUnit = $preparationTimeUnit;
        $instance->difficulty = $difficulty;
        $instance->description = $description;
        $instance->recipeIngredients = $recipeIngredient;
        $instance->recipeInstructions = $recipeInstruction;
        $instance->willCook = $willCook;
        $instance->recipeCategories = $recipeCategories;
        $instance->comment = $comment;
        $instance->rating = $rating;
        $instance->favourite = $favourite;

        return $instance;
    }

    /**
     * @return int
     */
    public function getUserRecipeId(): int
    {
        return $this->userRecipeId;
    }

    /**
     * @param int $userRecipeId
     */
    public function setUserRecipeId(int $userRecipeId): void
    {
        $this->userRecipeId = $userRecipeId;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return int|null
     */
    public function getServings(): ?int
    {
        return $this->servings;
    }

    /**
     * @param int|null $servings
     */
    public function setServings(?int $servings): void
    {
        $this->servings = $servings;
    }

    /**
     * @return int|null
     */
    public function getCaloriesPerServing(): ?int
    {
        return $this->caloriesPerServing;
    }

    /**
     * @param int|null $caloriesPerServing
     */
    public function setCaloriesPerServing(?int $caloriesPerServing): void
    {
        $this->caloriesPerServing = $caloriesPerServing;
    }

    /**
     * @return string|null
     */
    public function getSource(): ?string
    {
        return $this->source;
    }

    /**
     * @param string|null $source
     */
    public function setSource(?string $source): void
    {
        $this->source = $source;
    }

    /**
     * @return int|null
     */
    public function getPreparationTime(): ?int
    {
        return $this->preparationTime;
    }

    /**
     * @param int|null $preparationTime
     */
    public function setPreparationTime(?int $preparationTime): void
    {
        $this->preparationTime = $preparationTime;
    }

    /**
     * @return RecipePreparationTimeUnitEnum|null
     */
    public function getPreparationTimeUnit(): ?RecipePreparationTimeUnitEnum
    {
        return $this->preparationTimeUnit;
    }

    /**
     * @param RecipePreparationTimeUnitEnum|null $preparationTimeUnit
     */
    public function setPreparationTimeUnit(?RecipePreparationTimeUnitEnum $preparationTimeUnit): void
    {
        $this->preparationTimeUnit = $preparationTimeUnit;
    }

    /**
     * @return int|null
     */
    public function getDifficulty(): ?int
    {
        return $this->difficulty;
    }

    /**
     * @param int|null $difficulty
     */
    public function setDifficulty(?int $difficulty): void
    {
        $this->difficulty = $difficulty;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return RecipeIngredient[]|null
     */
    public function getRecipeIngredients(): ?array
    {
        return $this->recipeIngredients;
    }

    /**
     * @param RecipeIngredient[]|null $recipeIngredients
     */
    public function setRecipeIngredients(?array $recipeIngredients): void
    {
        $this->recipeIngredients = $recipeIngredients;
    }

    /**
     * @return RecipeInstruction[]|null
     */
    public function getRecipeInstructions(): ?array
    {
        return $this->recipeInstructions;
    }

    /**
     * @param RecipeInstruction[]|null $recipeInstructions
     */
    public function setRecipeInstructions(?array $recipeInstructions): void
    {
        $this->recipeInstructions = $recipeInstructions;
    }

    /**
     * @return bool
     */
    public function isWillCook(): bool
    {
        return $this->willCook;
    }

    /**
     * @param bool $willCook
     */
    public function setWillCook(bool $willCook): void
    {
        $this->willCook = $willCook;
    }

    /**
     * @return int[]|null
     */
    public function getRecipeCategories(): ?array
    {
        return $this->recipeCategories;
    }

    /**
     * @param int[]|null $recipeCategories
     */
    public function setRecipeCategories(?array $recipeCategories): void
    {
        $this->recipeCategories = $recipeCategories;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string|null $comment
     */
    public function setComment(?string $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return int|null
     */
    public function getRating(): ?int
    {
        return $this->rating;
    }

    /**
     * @param int|null $rating
     */
    public function setRating(?int $rating): void
    {
        $this->rating = $rating;
    }

    /**
     * @return bool
     */
    public function isFavourite(): bool
    {
        return $this->favourite;
    }

    /**
     * @param bool $favourite
     */
    public function setFavourite(bool $favourite): void
    {
        $this->favourite = $favourite;
    }
}