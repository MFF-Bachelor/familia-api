<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


use App\Application\Model\Entities\ShoppingListItem;
use DateTimeImmutable;

class CreateShoppingListInput
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string|null
     */
    private $description;

    /**
     * @var DateTimeImmutable|null
     */
    protected $dueTime;

    /**
     * @var string|null
     */
    private $assigneeUid;

    /**
     * @var ShoppingListItem[]|null
     */
    private $shoppingListItems;

    /**
     * @param string $title
     * @param string|null $description
     * @param DateTimeImmutable|null $dueTime
     * @param string|null $assigneeUid
     * @param ShoppingListItem[]|null $shoppingListItems
     * @return CreateShoppingListInput
     */
    public static function create(
        string $title,
        ?string $description,
        ?DateTimeImmutable $dueTime,
        ?string $assigneeUid,
        ?array $shoppingListItems
    ) : CreateShoppingListInput
    {
        $instance = new self();

        $instance->title = $title;
        $instance->description = $description;
        $instance->dueTime = $dueTime;
        $instance->assigneeUid = $assigneeUid;
        $instance->shoppingListItems = $shoppingListItems;

        return $instance;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getDueTime(): ?DateTimeImmutable
    {
        return $this->dueTime;
    }

    /**
     * @param DateTimeImmutable|null $dueTime
     */
    public function setDueTime(?DateTimeImmutable $dueTime): void
    {
        $this->dueTime = $dueTime;
    }

    /**
     * @return string|null
     */
    public function getAssigneeUid(): ?string
    {
        return $this->assigneeUid;
    }

    /**
     * @param string|null $assigneeUid
     */
    public function setAssigneeUid(?string $assigneeUid): void
    {
        $this->assigneeUid = $assigneeUid;
    }

    /**
     * @return ShoppingListItem[]|null
     */
    public function getShoppingListItems(): ?array
    {
        return $this->shoppingListItems;
    }

    /**
     * @param ShoppingListItem[]|null $shoppingListItems
     */
    public function setShoppingListItems(?array $shoppingListItems): void
    {
        $this->shoppingListItems = $shoppingListItems;
    }

}