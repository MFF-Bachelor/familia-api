<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


use App\Application\Model\Entities\Recipe;
use App\Application\Model\Entities\RecipeIngredient;
use App\Application\Model\Entities\RecipeInstruction;
use App\Application\Model\Entities\ShoppingListItem;
use App\Application\Model\Entities\User;
use App\Application\Model\Enum\GenderEnum;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\RecipeIngredientUnitEnum;
use App\Application\Model\Enum\RecipePreparationTimeUnitEnum;
use App\Application\Model\Enum\RelationshipEnum;
use App\Application\Model\Enum\RelationshipSideEnum;
use App\Application\Model\Enum\RelationshipStateEnum;
use App\Application\Model\Enum\TaskStateEnum;
use App\Application\Model\Enum\UserContactLabelEnum;
use App\Application\Model\Enum\UserContactTypeEnum;
use DateTimeImmutable;
use Exception;
use TheCodingMachine\GraphQLite\Annotations\Factory;
use TheCodingMachine\GraphQLite\Annotations\UseInputType;

/**
 * Class InputTypesFactory
 * @package App\Application\Model\GraphQLInputTypes
 */
class InputTypesFactory
{
    /**
     * @Factory(name="CreateUserInput")
     *
     * @param string $uid
     * @param EntityStateEnum|null $state
     * @param string|null $firstName
     * @param string|null $lastName
     * @param string[]|null $emails
     * @param string[]|null $phoneNumbers
     * @param DateTimeImmutable|null $birthDate
     * @param GenderEnum|null $gender
     * @param string|null $street
     * @param string|null $descriptiveNumber
     * @param string|null $routingNumber
     * @param string|null $postcode
     * @param string|null $city
     * @param string|null $country
     * @return CreateUserInput
     */
    public function createUser(
        string $uid,
        ?EntityStateEnum $state,
        ?string $firstName,
        ?string $lastName,
        ?array $emails,
        ?array $phoneNumbers,
        ?DateTimeImmutable $birthDate,
        ?GenderEnum $gender,
        ?string $street,
        ?string $descriptiveNumber,
        ?string $routingNumber,
        ?string $postcode,
        ?string $city,
        ?string $country
    ) : CreateUserInput
    {
        return CreateUserInput::create(
            $uid,
            $state,
            $firstName,
            $lastName,
            $emails,
            $phoneNumbers,
            $birthDate,
            $gender,
            $street,
            $descriptiveNumber,
            $routingNumber,
            $postcode,
            $city,
            $country
        );
    }

    /**
     * @Factory(name="UpdateUserInput")
     *
     * @param string|null $uid
     * @param EntityStateEnum|null $state
     * @param string|null $firstName
     * @param string|null $lastName
     * @param string[]|null $emails
     * @param string[]|null $phoneNumbers
     * @param DateTimeImmutable|null $birthDate
     * @param GenderEnum|null $gender
     * @param string|null $street
     * @param string|null $descriptiveNumber
     * @param string|null $routingNumber
     * @param string|null $postcode
     * @param string|null $city
     * @param string|null $country
     * @return UpdateUserInput
     */
    public function updateUser(
        ?string $uid,
        ?EntityStateEnum $state,
        ?string $firstName,
        ?string $lastName,
        ?array $emails,
        ?array $phoneNumbers,
        ?DateTimeImmutable $birthDate,
        ?GenderEnum $gender,
        ?string $street,
        ?string $descriptiveNumber,
        ?string $routingNumber,
        ?string $postcode,
        ?string $city,
        ?string $country
    ) : UpdateUserInput
    {
        return UpdateUserInput::create(
            $uid,
            $state,
            $firstName,
            $lastName,
            $emails,
            $phoneNumbers,
            $birthDate,
            $gender,
            $street,
            $descriptiveNumber,
            $routingNumber,
            $postcode,
            $city,
            $country
        );
    }

    /**
     * @Factory(name="LoginUserInput")
     *
     * @param string $token
     * @return LoginUserInput
     */
    public function loginUser(string $token) : LoginUserInput {
        return LoginUserInput::create($token);
    }

    /**
     * @Factory(name="CreateTwoWayRelationshipInput")
     *
     * @param string $userToEmail
     * @param RelationshipEnum $role
     * @param GenderEnum|null $gender
     * @param RelationshipSideEnum|null $side
     * @param string|null $bio
     * @return CreateTwoWayRelationshipInput
     */
    public function createTwoWayRelationshipInput(
        string $userToEmail,
        RelationshipEnum $role,
        ?GenderEnum $gender,
        ?RelationshipSideEnum $side,
        ?string $bio
    ) : CreateTwoWayRelationshipInput
    {
        return CreateTwoWayRelationshipInput::create(
            $userToEmail,
            $role,
            $gender,
            $side,
            $bio
        );
    }

    /**
     * @Factory(name="CreateOneWayRelationshipInput")
     *
     * @param string|null $firstName
     * @param string|null $lastName
     * @param DateTimeImmutable|null $birthDate
     * @param GenderEnum|null $gender
     * @param RelationshipEnum $role
     * @param RelationshipSideEnum|null $side
     * @param string|null $bio
     * @return CreateOneWayRelationshipInput
     */
    public function createOneWayRelationshipInput(
        ?string $firstName,
        ?string $lastName,
        ?DateTimeImmutable $birthDate,
        ?GenderEnum $gender,
        RelationshipEnum $role,
        ?RelationshipSideEnum $side,
        ?string $bio
    ) : CreateOneWayRelationshipInput
    {
        return CreateOneWayRelationshipInput::create(
            $firstName,
            $lastName,
            $birthDate,
            $gender,
            $role,
            $side,
            $bio
        );
    }

    /**
     * @Factory(name="UpdateOneWayRelationshipInput")
     *
     * @param int $relationshipId
     * @param string|null $firstName
     * @param string|null $lastName
     * @param DateTimeImmutable|null $birthDate
     * @param GenderEnum|null $gender
     * @param RelationshipEnum|null $role
     * @param RelationshipSideEnum|null $side
     * @param string|null $bio
     * @return UpdateOneWayRelationshipInput
     */
    public function updateOneWayRelationshipInput(
        int $relationshipId,
        ?string $firstName,
        ?string $lastName,
        ?DateTimeImmutable $birthDate,
        ?GenderEnum $gender,
        ?RelationshipEnum $role,
        ?RelationshipSideEnum $side,
        ?string $bio
    ) : UpdateOneWayRelationshipInput
    {
        return UpdateOneWayRelationshipInput::create(
            $relationshipId,
            $firstName,
            $lastName,
            $birthDate,
            $gender,
            $role,
            $side,
            $bio
        );
    }

    /**
     * @Factory(name="UpdateTwoWayRelationshipInput")
     *
     * @param int $relationshipId
     * @param RelationshipSideEnum|null $side
     * @param string|null $bio
     * @return UpdateTwoWayRelationshipInput
     */
    public function updateTwoWayRelationshipInput(
        int $relationshipId,
        ?RelationshipSideEnum $side,
        ?string $bio
    ) : UpdateTwoWayRelationshipInput
    {
        return UpdateTwoWayRelationshipInput::create(
            $relationshipId,
            $side,
            $bio
        );
    }

    /**
     * @Factory(name="CreateTaskInput")
     *
     * @UseInputType(for="$assigneeUids", inputType="[String!]!")
     *
     * @param string $title
     * @param string|null $text
     * @param DateTimeImmutable|null $dueTime
     * @param array $assigneeUids
     * @param int $priority
     * @param TaskStateEnum $state
     * @param bool $canAssigneeEdit
     * @return CreateTaskInput
     */
    public function createTaskInput(
        string $title,
        ?string $text,
        ?DateTimeImmutable $dueTime,
        array $assigneeUids,
        int $priority,
        TaskStateEnum $state,
        bool $canAssigneeEdit
    ) : CreateTaskInput
    {
        return CreateTaskInput::create(
            $title,
            $text,
            $dueTime,
            $assigneeUids,
            $priority,
            $state,
            $canAssigneeEdit
        );
    }

    /**
     * @Factory(name="SetTaskInput")
     *
     * @param int $taskId
     * @param string $title
     * @param string|null $text
     * @param DateTimeImmutable|null $dueTime
     * @param string $assigneeUid
     * @param int $priority
     * @param TaskStateEnum $state
     * @param bool $canAssigneeEdit
     * @return SetTaskInput
     */
    public function setTaskInput(
        int $taskId,
        string $title,
        ?string $text,
        ?DateTimeImmutable $dueTime,
        string $assigneeUid,
        int $priority,
        TaskStateEnum $state,
        bool $canAssigneeEdit
    ) : SetTaskInput
    {
        return SetTaskInput::create(
            $taskId,
            $title,
            $text,
            $dueTime,
            $assigneeUid,
            $priority,
            $state,
            $canAssigneeEdit
        );
    }

    /**
     * @Factory(name="RecipeIngredientInput")
     *
     * @param int|null $amount
     * @param RecipeIngredientUnitEnum|null $unit
     * @param string|null $name
     * @return RecipeIngredient
     */
    public function createRecipeIngredientsInput(
        ?int $amount,
        ?RecipeIngredientUnitEnum $unit,
        ?string $name
    ) : RecipeIngredient {
        return RecipeIngredient::create(
            $amount,
            $unit,
            $name
        );
    }

    /**
     * @Factory(name="RecipeInstructionInput")
     *
     * @param string|null $description
     * @param int|null $ordering
     * @return RecipeInstruction
     */
    public function createRecipeInstructionsInput(
        ?string $description,
        ?int $ordering
    ) : RecipeInstruction {
        return RecipeInstruction::create(
            $description,
            $ordering
        );
    }

    /**
     * @Factory(name="CreateRecipeInput")
     *
     * @param string $title
     * @param int|null $servings
     * @param int|null $caloriesPerServing
     * @param string|null $source
     * @param int|null $preparationTime
     * @param RecipePreparationTimeUnitEnum|null $preparationTimeUnit
     * @param int|null $difficulty
     * @param string|null $description
     * @param RecipeIngredient[]|null $recipeIngredients
     * @param RecipeInstruction[]|null $recipeInstructions
     * @param bool $willCook
     * @param int[]|null $recipeCategories
     * @param string $creator
     * @return CreateRecipeInput
     *
     * @UseInputType(for="$recipeIngredients", inputType="[RecipeIngredientInput!]")
     * @UseInputType(for="recipeInstructions", inputType="[RecipeInstructionInput!]")
     */
    public function CreateRecipeInput(
        string $title,
        ?int $servings,
        ?int $caloriesPerServing,
        ?string $source,
        ?int $preparationTime,
        ?RecipePreparationTimeUnitEnum $preparationTimeUnit,
        ?int $difficulty,
        ?string $description,
        ?array $recipeIngredients,
        ?array $recipeInstructions,
        bool $willCook,
        ?array $recipeCategories,
        string $creator
    ): CreateRecipeInput
    {
        return CreateRecipeInput::create(
            $title,
            $servings,
            $caloriesPerServing,
            $source,
            $preparationTime,
            $preparationTimeUnit,
            $difficulty,
            $description,
            $recipeIngredients,
            $recipeInstructions,
            $willCook,
            $recipeCategories,
            $creator
        );
    }

    /**
     * @Factory(name="SetRecipeInput")
     *
     * @param int $recipeId
     * @param string $title
     * @param int|null $servings
     * @param int|null $caloriesPerServing
     * @param string|null $source
     * @param int|null $preparationTime
     * @param RecipePreparationTimeUnitEnum|null $preparationTimeUnit
     * @param int|null $difficulty
     * @param string|null $description
     * @param RecipeIngredient[]|null $recipeIngredients
     * @param RecipeInstruction[]|null $recipeInstructions
     * @param bool $willCook
     * @param int[]|null $recipeCategories
     * @param string|null $comment
     * @param int|null $rating
     * @param bool|null $favourite
     * @return SetRecipeInput
     *
     * @UseInputType(for="$recipeIngredients", inputType="[RecipeIngredientInput!]")
     * @UseInputType(for="recipeInstructions", inputType="[RecipeInstructionInput!]")
     */
    public function SetRecipeInput(
        int $recipeId,
        string $title,
        ?int $servings,
        ?int $caloriesPerServing,
        ?string $source,
        ?int $preparationTime,
        ?RecipePreparationTimeUnitEnum $preparationTimeUnit,
        ?int $difficulty,
        ?string $description,
        ?array $recipeIngredients,
        ?array $recipeInstructions,
        bool $willCook,
        ?array $recipeCategories,
        ?string $comment,
        ?int $rating,
        ?bool $favourite
    ): SetRecipeInput
    {
        return SetRecipeInput::create(
            $recipeId,
            $title,
            $servings,
            $caloriesPerServing,
            $source,
            $preparationTime,
            $preparationTimeUnit,
            $difficulty,
            $description,
            $recipeIngredients,
            $recipeInstructions,
            $willCook,
            $recipeCategories,
            $comment,
            $rating,
            $favourite
        );
    }

    /**
     * @Factory(name="CreateUserContactInput")
     *
     * @param string|null $userUid
     * @param string|null $email
     * @param string|null $phoneNumber
     * @param UserContactLabelEnum $label
     * @param string $name
     * @param EntityStateEnum $state
     * @return CreateUserContactInput
     */
    public function CreateUserContactInput(
        ?string $userUid,
        ?string $email,
        ?string $phoneNumber,
        UserContactLabelEnum $label,
        string $name,
        EntityStateEnum $state
    ): CreateUserContactInput
    {
        return CreateUserContactInput::create(
            $userUid,
            $email,
            $phoneNumber,
            $label,
            $name,
            $state
        );
    }

    /**
     * @Factory(name="SetUserContactInput")
     *
     * @param string $userContactId
     * @param string $email
     * @param string $phoneNumber
     * @param UserContactLabelEnum $label
     * @param string $name
     * @param EntityStateEnum $state
     * @return SetUserContactInput
     */
    public function SetUserContactInput(
        string $userContactId,
        string $email,
        string $phoneNumber,
        UserContactLabelEnum $label,
        string $name,
        EntityStateEnum $state
    ): SetUserContactInput
    {
        return SetUserContactInput::create(
            $userContactId,
            $email,
            $phoneNumber,
            $label,
            $name,
            $state
        );
    }

    /**
     * @Factory(name="ShoppingListItemInput")
     *
     * @param string|null $title
     * @param bool|null $checked
     * @return ShoppingListItem
     */
    public function createShoppingListItemInput(
        ?string $title,
        ?bool $checked
    ) : ShoppingListItem {
        return ShoppingListItem::create(
            $title,
            $checked
        );
    }

    /**
     * @Factory(name="CreateShoppingListInput")
     *
     * @param string $title
     * @param string|null $description
     * @param DateTimeImmutable|null $dueTime
     * @param string|null $assigneeUid
     * @param ShoppingListItem[]|null $shoppingListItems
     *
     * @UseInputType(for="shoppingListItems", inputType="[ShoppingListItemInput!]")
     *
     * @return CreateShoppingListInput
     */
    public function createShoppingListInput(
        string $title,
        ?string $description,
        ?DateTimeImmutable $dueTime,
        ?string $assigneeUid,
        ?array $shoppingListItems
    ) : CreateShoppingListInput
    {
        return CreateShoppingListInput::create(
            $title,
            $description,
            $dueTime,
            $assigneeUid,
            $shoppingListItems
        );
    }

    /**
     * @Factory(name="SetShoppingListInput")
     *
     * @param int $shoppingListId
     * @param string $title
     * @param string|null $description
     * @param DateTimeImmutable|null $dueTime
     * @param string|null $assigneeUid
     * @param ShoppingListItem[]|null $shoppingListItems
     *
     * @return SetShoppingListInput
     * @UseInputType(for="shoppingListItems", inputType="[ShoppingListItemInput!]")
     *
     */
    public function setShoppingListInput(
        int $shoppingListId,
        string $title,
        ?string $description,
        ?DateTimeImmutable $dueTime,
        ?string $assigneeUid,
        ?array $shoppingListItems
    ) : SetShoppingListInput
    {
        return SetShoppingListInput::create(
            $shoppingListId,
            $title,
            $description,
            $dueTime,
            $assigneeUid,
            $shoppingListItems
        );
    }
}