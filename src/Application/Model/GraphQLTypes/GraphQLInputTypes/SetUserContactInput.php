<?php

namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;

use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\TaskStateEnum;
use App\Application\Model\Enum\UserContactLabelEnum;
use App\Application\Model\Enum\UserContactTypeEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateTaskInput;
use DateTimeImmutable;

class SetUserContactInput
{
    /**
     * @var int
     */
    private $userContactId;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $phoneNumber;

    /**
     * @var UserContactLabelEnum
     */
    private $label;

    /**
     * @var string
     */
    private $name;


    /**
     * @var EntityStateEnum
     */
    private $state;

    /**
     * @param string $userContactId
     * @param string $email
     * @param string $phoneNumber
     * @param UserContactLabelEnum $label
     * @param string $name
     * @param EntityStateEnum $state
     * @return SetUserContactInput
     */
    public static function create(
        string $userContactId,
        string $email,
        string $phoneNumber,
        UserContactLabelEnum $label,
        string $name,
        EntityStateEnum $state
    ) : SetUserContactInput
    {
        $instance = new self();

        $instance->userContactId = $userContactId;
        $instance->email = $email;
        $instance->phoneNumber = $phoneNumber;
        $instance->label = $label;
        $instance->name = $name;
        $instance->state = $state;

        return $instance;
    }

    /**
     * @return int
     */
    public function getUserContactId(): int
    {
        return $this->userContactId;
    }

    /**
     * @param int $userContactId
     */
    public function setUserContactId(int $userContactId): void
    {
        $this->userContactId = $userContactId;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPhoneNumber(): string
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     */
    public function setPhoneNumber(string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return UserContactLabelEnum
     */
    public function getLabel(): UserContactLabelEnum
    {
        return $this->label;
    }

    /**
     * @param UserContactLabelEnum $label
     */
    public function setLabel(UserContactLabelEnum $label): void
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return EntityStateEnum
     */
    public function getState(): EntityStateEnum
    {
        return $this->state;
    }

    /**
     * @param EntityStateEnum $state
     */
    public function setState(EntityStateEnum $state): void
    {
        $this->state = $state;
    }

}