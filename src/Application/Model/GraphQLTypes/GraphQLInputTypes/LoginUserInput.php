<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


class LoginUserInput
{
    /**
     * @var string
     */
    private $token;

    public static function create(string $token)
    {
        $instance = new self();

        $instance->token = $token;

        return $instance;
    }

    /**
     * @return string
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken(string $token): void
    {
        $this->token = $token;
    }
}