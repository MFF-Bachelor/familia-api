<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;

use App\Application\Model\Enum\GenderEnum;
use App\Application\Model\Enum\RelationshipEnum;
use App\Application\Model\Enum\RelationshipSideEnum;

class CreateTwoWayRelationshipInput
{
    /**
     * @var string
     */
    private $userToEmail;

    /**
     * @var RelationshipEnum
     */
    private $role;

    /**
     * @var GenderEnum|null
     */
    private $gender;

    /**
     * @var RelationshipSideEnum|null
     */
    private $side;

    /**
     * @var string|null
     */
    private $bio;

    /**
     * @param string $userToEmail
     * @param RelationshipEnum $role
     * @param GenderEnum|null $gender
     * @param RelationshipSideEnum|null $side
     * @param string|null $bio
     * @return CreateTwoWayRelationshipInput
     */
    public static function create(
        string $userToEmail,
        RelationshipEnum $role,
        ?GenderEnum $gender,
        ?RelationshipSideEnum $side,
        ?string $bio
    ) : CreateTwoWayRelationshipInput
    {
        $instance = new self();

        $instance->userToEmail = $userToEmail;
        $instance->role = $role;
        $instance->side = $side;
        $instance->gender = $gender;
        $instance->bio = $bio;

        return $instance;
    }

    /**
     * @return string
     */
    public function getUserToEmail(): string
    {
        return $this->userToEmail;
    }

    /**
     * @param string $userToEmail
     */
    public function setUserToEmail(string $userToEmail): void
    {
        $this->userToEmail = $userToEmail;
    }

    /**
     * @return RelationshipEnum
     */
    public function getRole(): RelationshipEnum
    {
        return $this->role;
    }

    /**
     * @param RelationshipEnum $role
     */
    public function setRole(RelationshipEnum $role): void
    {
        $this->role = $role;
    }

    /**
     * @return RelationshipSideEnum|null
     */
    public function getSide(): ?RelationshipSideEnum
    {
        return $this->side;
    }

    /**
     * @param RelationshipSideEnum|null $side
     */
    public function setSide(?RelationshipSideEnum $side): void
    {
        $this->side = $side;
    }

    /**
     * @return GenderEnum|null
     */
    public function getGender(): ?GenderEnum
    {
        return $this->gender;
    }

    /**
     * @param GenderEnum|null $gender
     */
    public function setGender(?GenderEnum $gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return string|null
     */
    public function getBio(): ?string
    {
        return $this->bio;
    }

    /**
     * @param string|null $bio
     */
    public function setBio(?string $bio): void
    {
        $this->bio = $bio;
    }
}