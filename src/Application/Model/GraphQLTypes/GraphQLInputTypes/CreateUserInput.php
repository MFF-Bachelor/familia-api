<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


use App\Application\Model\Enum\GenderEnum;
use App\Application\Model\Enum\EntityStateEnum;
use DateTimeImmutable;

class CreateUserInput
{
    /**
     * @var string
     */
    private $uid;

    /**
     * @var EntityStateEnum
     */
    private $state;

    /**
     * @var string|null
     */
    private $firstName;

    /**
     * @var string|null
     */
    private $lastName;

    /**
     * @var string[]|null
     */
    private $emails;

    /**
     * @var string[]|null
     */
    private $phoneNumbers;

    /**
     * @var DateTimeImmutable|null
     */
    private $birthDate;

    /**
     * @var GenderEnum|null
     */
    private $gender;

    /**
     * @var string|null
     */
    private $street;

    /**
     * @var string|null
     */
    private $descriptiveNumber;

    /**
     * @var string|null
     */
    private $routingNumber;

    /**
     * @var string|null
     */
    private $postcode;

    /**
     * @var string|null
     */
    private $city;

    /**
     * @var string|null
     */
    private $country;


    /**
     * @param string $uid
     * @param EntityStateEnum|null $state
     * @param string|null $firstName
     * @param string|null $lastName
     * @param string[]|null $emails
     * @param string[]|null $phoneNumbers
     * @param DateTimeImmutable|null $birthDate
     * @param GenderEnum|null $gender
     * @param string|null $street
     * @param string|null $descriptiveNumber
     * @param string|null $routingNumber
     * @param string|null $postcode
     * @param string|null $city
     * @param string|null $country
     * @return CreateUserInput
     */
    public static function create(
        string $uid,
        ?EntityStateEnum $state,
        ?string $firstName,
        ?string $lastName,
        ?array $emails,
        ?array $phoneNumbers,
        ?DateTimeImmutable $birthDate,
        ?GenderEnum $gender,
        ?string $street,
        ?string $descriptiveNumber,
        ?string $routingNumber,
        ?string $postcode,
        ?string $city,
        ?string $country
    ) : CreateUserInput
    {
        $instance = new self();

        $instance->uid = $uid;
        $instance->state = $state;
        $instance->firstName = $firstName;
        $instance->lastName = $lastName;
        $instance->emails = $emails;
        $instance->phoneNumbers = $phoneNumbers;
        $instance->birthDate = $birthDate;
        $instance->gender = $gender;
        $instance->street = $street;
        $instance->descriptiveNumber = $descriptiveNumber;
        $instance->routingNumber = $routingNumber;
        $instance->postcode = $postcode;
        $instance->city = $city;
        $instance->country = $country;

        return $instance;
    }

    /**
     * @return string
     */
    public function getUid(): string
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     */
    public function setUid(string $uid): void
    {
        $this->uid = $uid;
    }

    /**
     * @return EntityStateEnum
     */
    public function getState(): EntityStateEnum
    {
        return $this->state;
    }

    /**
     * @param EntityStateEnum $state
     */
    public function setState(EntityStateEnum $state): void
    {
        $this->state = $state;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     */
    public function setFirstName(?string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     */
    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string[]|null
     */
    public function getEmails(): ?array
    {
        return $this->emails;
    }

    /**
     * @param string[] $emails
     */
    public function setEmails(array $emails): void
    {
        $this->emails = $emails;
    }

    /**
     * @return string[]|null
     */
    public function getPhoneNumbers(): ?array
    {
        return $this->phoneNumbers;
    }

    /**
     * @param string[] $phoneNumbers
     */
    public function setPhoneNumbers(array $phoneNumbers): void
    {
        $this->phoneNumbers = $phoneNumbers;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getBirthDate(): ?DateTimeImmutable
    {
        return $this->birthDate;
    }

    /**
     * @param DateTimeImmutable|null $birthDate
     */
    public function setBirthDate(?DateTimeImmutable $birthDate): void
    {
        $this->birthDate = $birthDate;
    }

    /**
     * @return GenderEnum|null
     */
    public function getGender(): ?GenderEnum
    {
        return $this->gender;
    }

    /**
     * @param GenderEnum|null $gender
     */
    public function setGender(?GenderEnum $gender): void
    {
        $this->gender = $gender;
    }

    /**
     * @return string|null
     */
    public function getStreet(): ?string
    {
        return $this->street;
    }

    /**
     * @param string|null $street
     */
    public function setStreet(?string $street): void
    {
        $this->street = $street;
    }

    /**
     * @return string|null
     */
    public function getDescriptiveNumber(): ?string
    {
        return $this->descriptiveNumber;
    }

    /**
     * @param string|null $descriptiveNumber
     */
    public function setDescriptiveNumber(?string $descriptiveNumber): void
    {
        $this->descriptiveNumber = $descriptiveNumber;
    }

    /**
     * @return string|null
     */
    public function getRoutingNumber(): ?string
    {
        return $this->routingNumber;
    }

    /**
     * @param string|null $routingNumber
     */
    public function setRoutingNumber(?string $routingNumber): void
    {
        $this->routingNumber = $routingNumber;
    }

    /**
     * @return string|null
     */
    public function getPostcode(): ?string
    {
        return $this->postcode;
    }

    /**
     * @param string|null $postcode
     */
    public function setPostcode(?string $postcode): void
    {
        $this->postcode = $postcode;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     */
    public function setCity(?string $city): void
    {
        $this->city = $city;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     */
    public function setCountry(?string $country): void
    {
        $this->country = $country;
    }
}