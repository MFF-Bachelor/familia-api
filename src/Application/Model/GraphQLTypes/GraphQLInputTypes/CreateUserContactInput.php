<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;

use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\UserContactLabelEnum;
use App\Application\Model\Enum\UserContactTypeEnum;

class CreateUserContactInput
{
    /**
     * @var string|null
     */
    private $userUid;

    /**
     * @var string|null
     */
    private $email;

    /**
     * @var string|null
     */
    private $phoneNumber;

    /**
     * @var UserContactLabelEnum
     */
    private $label;

    /**
     * @var string
     */
    private $name;


    /**
     * @var EntityStateEnum
     */
    private $state;

    /**
     * @param string|null $userUid
     * @param string|null $email
     * @param string|null $phoneNumber
     * @param UserContactLabelEnum $label
     * @param string $name
     * @param EntityStateEnum $state
     * @return CreateUserContactInput
     */
    public static function create(
        ?string $userUid,
        ?string $email,
        ?string $phoneNumber,
        UserContactLabelEnum $label,
        string $name,
        EntityStateEnum $state

    ): CreateUserContactInput
    {
        $instance = new self();

        $instance->userUid = $userUid;
        $instance->email = $email;
        $instance->phoneNumber = $phoneNumber;
        $instance->label = $label;
        $instance->name = $name;
        $instance->state = $state;

        return $instance;
    }

    /**
     * @return UserContactLabelEnum
     */
    public function getLabel(): UserContactLabelEnum
    {
        return $this->label;
    }

    /**
     * @param UserContactLabelEnum $label
     */
    public function setLabel(UserContactLabelEnum $label): void
    {
        $this->label = $label;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return EntityStateEnum
     */
    public function getState(): EntityStateEnum
    {
        return $this->state;
    }

    /**
     * @param EntityStateEnum $state
     */
    public function setState(EntityStateEnum $state): void
    {
        $this->state = $state;
    }

    /**
     * @return string|null
     */
    public function getUserUid(): ?string
    {
        return $this->userUid;
    }

    /**
     * @param string|null $userUid
     */
    public function setUserUid(?string $userUid): void
    {
        $this->userUid = $userUid;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string|null
     */
    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    /**
     * @param string|null $phoneNumber
     */
    public function setPhoneNumber(?string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }
}