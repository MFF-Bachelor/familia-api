<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


use App\Application\Model\Enum\GenderEnum;
use App\Application\Model\Enum\RelationshipEnum;
use App\Application\Model\Enum\RelationshipSideEnum;

class UpdateTwoWayRelationshipInput
{
    /**
     * @var int
     */
    private $relationshipId;

    /**
     * @var RelationshipSideEnum|null
     */
    private $side;

    /**
     * @var string|null
     */
    private $bio;

    /**
     * @param int $relationshipId
     * @param RelationshipSideEnum|null $side
     * @param string|null $bio
     * @return UpdateTwoWayRelationshipInput
     */
    public static function create(
        int $relationshipId,
        ?RelationshipSideEnum $side,
        ?string $bio
    ) : UpdateTwoWayRelationshipInput
    {
        $instance = new self();

        $instance->relationshipId = $relationshipId;
        $instance->side = $side;
        $instance->bio = $bio;

        return $instance;
    }

    /**
     * @return int
     */
    public function getRelationshipId(): int
    {
        return $this->relationshipId;
    }

    /**
     * @param int $relationshipId
     */
    public function setRelationshipId(int $relationshipId): void
    {
        $this->relationshipId = $relationshipId;
    }

    /**
     * @return RelationshipSideEnum|null
     */
    public function getSide(): ?RelationshipSideEnum
    {
        return $this->side;
    }

    /**
     * @param RelationshipSideEnum|null $side
     */
    public function setSide(?RelationshipSideEnum $side): void
    {
        $this->side = $side;
    }

    /**
     * @return string|null
     */
    public function getBio(): ?string
    {
        return $this->bio;
    }

    /**
     * @param string|null $bio
     */
    public function setBio(?string $bio): void
    {
        $this->bio = $bio;
    }
}