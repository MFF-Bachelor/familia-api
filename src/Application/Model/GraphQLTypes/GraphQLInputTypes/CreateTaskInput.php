<?php


namespace App\Application\Model\GraphQLTypes\GraphQLInputTypes;


use App\Application\Model\Enum\TaskStateEnum;
use DateTimeImmutable;

class CreateTaskInput
{
    /**
     * @var string
     */
    private $title;

    /**
     * @var string|null
     */
    private $text;

    /**
     * @var DateTimeImmutable|null
     */
    protected $dueTime;

    /**
     * @var string[]
     */
    private $assigneeUids;

    /**
     * @var int
     */
    private $priority;

    /**
     * @var TaskStateEnum
     */
    private $state;

    /**
     * @var boolean
     */
    private $canAssigneeEdit;

    /**
     * @param string $title
     * @param string $text
     * @param DateTimeImmutable $dueTime
     * @param string[] $assigneeUids
     * @param int $priority
     * @param TaskStateEnum $state
     * @return CreateTaskInput
     */
    public static function create(
        string $title,
        ?string $text,
        ?DateTimeImmutable $dueTime,
        array $assigneeUids,
        int $priority,
        TaskStateEnum $state,
        bool  $canAssigneeEdit
    ) : CreateTaskInput
    {
        $instance = new self();

        $instance->title = $title;
        $instance->text = $text;
        $instance->dueTime = $dueTime;
        $instance->assigneeUids = $assigneeUids;
        $instance->state = $state;
        $instance->priority = $priority;
        $instance->canAssigneeEdit = $canAssigneeEdit;

        return $instance;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return DateTimeImmutable|null
     */
    public function getDueTime(): ?DateTimeImmutable
    {
        return $this->dueTime;
    }

    /**
     * @param DateTimeImmutable|null $dueTime
     */
    public function setDueTime(?DateTimeImmutable $dueTime): void
    {
        $this->dueTime = $dueTime;
    }

    /**
     * @return int
     */
    public function getPriority(): int
    {
        return $this->priority;
    }

    /**
     * @param int $priority
     */
    public function setPriority(int $priority): void
    {
        $this->priority = $priority;
    }

    /**
     * @return TaskStateEnum
     */
    public function getState(): TaskStateEnum
    {
        return $this->state;
    }

    /**
     * @param TaskStateEnum $state
     */
    public function setState(TaskStateEnum $state): void
    {
        $this->state = $state;
    }

    /**
     * @return string[]
     */
    public function getAssigneeUids(): array
    {
        return $this->assigneeUids;
    }

    /**
     * @param string[] $assigneeUids
     */
    public function setAssigneeUids(array $assigneeUids): void
    {
        $this->assigneeUids = $assigneeUids;
    }

    /**
     * @return bool
     */
    public function isCanAssigneeEdit(): bool
    {
        return $this->canAssigneeEdit;
    }

    /**
     * @param bool $canAssigneeEdit
     */
    public function setCanAssigneeEdit(bool $canAssigneeEdit): void
    {
        $this->canAssigneeEdit = $canAssigneeEdit;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string|null $text
     */
    public function setText(?string $text): void
    {
        $this->text = $text;
    }
}