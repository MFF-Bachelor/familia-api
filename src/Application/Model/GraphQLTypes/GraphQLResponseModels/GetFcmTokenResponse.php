<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\FcmToken;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type
 *
 * Class GetFcmTokenResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetFcmTokenResponse extends GraphQLResponseModel
{
    /**
     * @var FcmToken|null
     */
    protected $data;

    /**
     * @Field()
     *
     * @return FcmToken|null
     */
    public function getData(): ?FcmToken
    {
        return $this->data;
    }

    /**
     * @param FcmToken|null $data
     */
    public function setData(?FcmToken $data): void
    {
        $this->data = $data;
    }
}