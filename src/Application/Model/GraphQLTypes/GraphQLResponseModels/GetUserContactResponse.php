<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\UserContact;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type
 *
 * Class GetContactResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetUserContactResponse extends GraphQLResponseModel
{
    /**
     * @var UserContact[]|null
     */
    protected $data;

    /**
     * @Field()
     * @return UserContact[]|null
     */
    public function getData(): ?iterable
    {
        return $this->data;
    }

    /**
     * @param iterable|null $data
     */
    public function setData(?iterable $data): void
    {
        $this->data = $data;
    }
}