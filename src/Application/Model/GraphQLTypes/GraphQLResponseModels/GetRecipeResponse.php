<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\Recipe;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type
 *
 * Class GetRecipeResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetRecipeResponse extends GraphQLResponseModel
{
    /**
     * @var Recipe[]|null
     */
    protected $data;

    /**
     * @Field()
     * @return Recipe[]|null
     */
    public function getData(): ?iterable
    {
        return $this->data;
    }

    /**
     * @param iterable|null $data
     */
    public function setData(?iterable $data): void
    {
        $this->data = $data;
    }
}