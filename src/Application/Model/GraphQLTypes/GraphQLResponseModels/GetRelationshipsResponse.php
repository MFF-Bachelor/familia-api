<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\Relationship;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type
 *
 * Class GetRelationshipsResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetRelationshipsResponse extends GraphQLResponseModel
{
    /**
     * @var Relationship[]|null
     */
    protected $data;

    /**
     * @Field()
     * @return Relationship[]|null
     */
    public function getData(): ?iterable
    {
        return $this->data;
    }

    /**
     * @param iterable|null $data
     */
    public function setData(?iterable $data): void
    {
        $this->data = $data;
    }
}