<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\Relationship;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type()
 *
 * Class GetRelationshipResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetRelationshipResponse extends GraphQLResponseModel
{
    /**
     * @var Relationship|null
     */
    protected $data;

    /**
     * @Field()
     *
     * @return Relationship|null
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param Relationship|null $data
     */
    public function setData(?Relationship $data): void
    {
        $this->data = $data;
    }
}