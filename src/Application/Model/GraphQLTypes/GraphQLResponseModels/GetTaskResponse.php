<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\Task;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type
 *
 * Class GetTaskResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetTaskResponse extends GraphQLResponseModel
{
    /**
     * @var Task[]|null
     */
    protected $data;

    /**
     * @Field()
     * @return Task[]|null
     */
    public function getData(): ?iterable
    {
        return $this->data;
    }

    /**
     * @param iterable|null $data
     */
    public function setData(?iterable $data): void
    {
        $this->data = $data;
    }
}