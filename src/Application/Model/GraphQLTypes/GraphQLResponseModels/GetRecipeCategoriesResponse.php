<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\RecipeCategory;
use App\Application\Model\Entities\User;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type()
 *
 * Class GetRecipeCategoriesResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetRecipeCategoriesResponse extends GraphQLResponseModel
{
    /**
     * @var RecipeCategory[]|null
     */
    protected $data;

    /**
     * @Field
     *
     * @return RecipeCategory[]|null
     */
    public function getData(): ?array
    {
        return $this->data;
    }

    /**
     * @param RecipeCategory[]|null $data
     */
    public function setData(?array $data): void
    {
        $this->data = $data;
    }


}