<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use App\Application\Model\Entities\ShoppingList;
use TheCodingMachine\GraphQLite\Annotations\Field;
use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type
 *
 * Class GetShoppingListResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class GetShoppingListResponse extends GraphQLResponseModel
{
    /**
     * @var ShoppingList[]|null
     */
    protected $data;

    /**
     * @Field()
     * @return ShoppingList[]|null
     */
    public function getData(): ?iterable
    {
        return $this->data;
    }

    /**
     * @param iterable|null $data
     */
    public function setData(?iterable $data): void
    {
        $this->data = $data;
    }
}