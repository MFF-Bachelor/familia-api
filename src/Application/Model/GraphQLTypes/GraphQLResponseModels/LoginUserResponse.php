<?php


namespace App\Application\Model\GraphQLTypes\GraphQLResponseModels;

use TheCodingMachine\GraphQLite\Annotations\Type;

/** GraphQLite annotations:
 * @Type()
 *
 * Class LoginUserResponse
 * @package App\Application\Model\GraphQLTypes\GraphQLResponseModels
 */
class LoginUserResponse extends GraphQLResponseModel
{

}