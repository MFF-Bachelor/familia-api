<?php


namespace App\Application;


use App\Application\Model\Entities\Language;
use App\Application\Model\Enum\LocaleEnum;
use App\Application\Model\Enum\RelationshipEnum;
use App\Application\Model\GraphQLTypes\Translation;

class Utils
{
    public static function GetOppositeRelationship(RelationshipEnum $relationship) {
        switch ($relationship) {
            case RelationshipEnum::PARENT:
                return RelationshipEnum::CHILD;
                break;
            case RelationshipEnum::CHILD:
                return RelationshipEnum::PARENT;
                break;
            case RelationshipEnum::FRIEND:
                return RelationshipEnum::FRIEND;
                break;
            case RelationshipEnum::UNMARRIED_PARTNER:
                return RelationshipEnum::UNMARRIED_PARTNER;
                break;
            case RelationshipEnum::PARTNER:
                return RelationshipEnum::PARTNER;
                break;
            case RelationshipEnum::COUSIN:
                return RelationshipEnum::COUSIN;
                break;
            case RelationshipEnum::GRAND_PARENT:
                return RelationshipEnum::GRAND_CHILD;
                break;
            case RelationshipEnum::GRAND_GRAND_PARENT:
                RelationshipEnum::GRAND_GRAND_CHILD;
                break;
            case RelationshipEnum::PARENT_IN_LAW:
                return null;
                break;
            case RelationshipEnum::GRAND_CHILD:
                return RelationshipEnum::GRAND_PARENT;
                break;
            case RelationshipEnum::GRAND_GRAND_CHILD:
                return RelationshipEnum::GRAND_GRAND_PARENT;
                break;
            case RelationshipEnum::SIBLING:
                return RelationshipEnum::SIBLING;
                break;
            case RelationshipEnum::STEP_SIBLING:
                return  RelationshipEnum::STEP_SIBLING;
                break;
            case RelationshipEnum::SIBLING_IN_LAW:
                return RelationshipEnum::SIBLING_IN_LAW;
                break;
            case RelationshipEnum::NEPHEW_NIECE:
                return RelationshipEnum::AUNT_UNCLE;
                break;
            case RelationshipEnum::AUNT_UNCLE:
                return RelationshipEnum::NEPHEW_NIECE;
                break;
        }
    }
}