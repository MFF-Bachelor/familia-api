<?php


namespace App\Application\Controllers\QueryControllers;


use App\Application\Model\Entities\Recipe;
use App\Application\Model\Entities\ShoppingList;
use App\Application\Model\Entities\ShoppingListItem;
use App\Application\Model\Entities\Task;
use App\Application\Model\Entities\User;
use App\Application\Model\Entities\UserRecipe;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\NotificationTypeEnum;
use App\Application\Model\Enum\TaskStateEnum;
use App\Application\Model\Enum\UserRecipeTypeEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateRecipeInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateShoppingListInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetShoppingListInput;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetRecipeResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetShoppingListResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\RemoveItemResponse;
use DateTimeZone;
use Exception;
use Kreait\Firebase\Auth;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\UseInputType;

/**
 * Class ShoppingListController. Resolves queries and mutations connected with ShoppingList entity and appropriate other entities
 *
 * @package App\Application\Controllers\QueryControllers
 */
class ShoppingListController extends BaseController
{
    private $firebase;
    protected $notificationController;

    /**
     * TaskController constructor. Sets main repository of class @see User
     */
    public function __construct()
    {
        parent::__construct();
        $this->firebase = $this->container->get(Auth::class);
        $this->notificationController = new NotificationController();
    }

    /**
     * CreateShoppingList mutation
     *
     * @Mutation(name="createShoppingList")
     *
     * @UseInputType(for="$inputData", inputType="CreateShoppingListInput")
     * @param CreateShoppingListInput $inputData
     * @return GetShoppingListResponse
     */
    public function createShoppingList(CreateShoppingListInput $inputData) : GetShoppingListResponse {
        $result = new GetShoppingListResponse();

        try {
            if (!isset($_SESSION['uid']))
                throw new Exception("Create failed: User in session does not exist.");

            /** @var User|null $creator */
            $creator = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));
            if (is_null($creator)) {
                $result->setData(null);
                throw new Exception("Create failed: User, who you can create a relationship does not exist in database");
            }

            /** @var User|null $assignee */
            $assignee = $this->userRepository->findOneBy((array('uid' => $inputData->getAssigneeUid())));

            $dueTime = null;
            if (!is_null($inputData->getDueTime()))
                $dueTime = $inputData->getDueTime()->setTimezone(new DateTimeZone('UTC'));

            $newShoppingList = ShoppingList::create(
                $inputData->getTitle(),
                $inputData->getDescription(),
                $inputData->getShoppingListItems(),
                $dueTime,
                $creator,
                $assignee,
                new EntityStateEnum(EntityStateEnum::ACTIVE),
                $this->entityManager
            );

            if (!is_null($assignee) && $creator->getUid() != $assignee->getUid()) {
                $this->notificationController->sendNotification(
                    $creator,
                    $assignee,
                    new NotificationTypeEnum(NotificationTypeEnum::ASSIGN_SHOPPING_LIST)
                );
            }

            $this->entityManager->persist($newShoppingList);
            $this->entityManager->flush();

            $result->setData($creator->getCreatedShoppingLists());
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * DeleteShoppingList mutation
     *
     * Makes given shopping list deleted
     *
     * @Mutation(name="deleteShoppingList")
     *
     * @param int $shoppingListId
     * @return RemoveItemResponse
     */
    public function deleteShoppingList(int $shoppingListId) : RemoveItemResponse {
        $result = new RemoveItemResponse();

        try {
            if (!isset($_SESSION['uid']))
                throw new Exception('Delete failed: No uid in session');

            /** @var User|null $loggedUser */
            $loggedUser = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));
            if (is_null($loggedUser)) {
                throw new Exception("Delete failed: logged user does not exist in database");
            }

            /** @var ShoppingList|null $shoppingList */
            $shoppingList = $this->shoppingListRepository->findOneBy((array('id' => $shoppingListId)));
            if (is_null($shoppingList)) {
                throw new Exception("Delete failed: ShoppingList does not exist in database");
            }

            if ($loggedUser === $shoppingList->getCreator()) {
                if (!is_null($shoppingList->getAssignee())) {
                    $shoppingList->setCreator($shoppingList->getAssignee());
                    $shoppingList->setAssignee(null);
                } else {
                    $shoppingList->setState(new EntityStateEnum(EntityStateEnum::DELETED));
                }
            }
            else if ($loggedUser === $shoppingList->getAssignee()) {
                if (!is_null($shoppingList->getCreator())) {
                    $shoppingList->setAssignee(null);
                } else {
                    $shoppingList->setState(new EntityStateEnum(EntityStateEnum::DELETED));
                }
            }
            else throw new Exception('Delete failed: Something went wrong.');

            $this->entityManager->persist($shoppingList);
            $this->entityManager->flush();

            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }


    /**
     * SetShoppingList mutation
     *
     * Sets new values to given shopping list
     *
     * @Mutation(name="setShoppingList")
     *
     * @UseInputType(for="$inputData", inputType="SetShoppingListInput")
     * @param SetShoppingListInput $inputData
     * @return GetShoppingListResponse
     */
    public function setShoppingList(SetShoppingListInput $inputData) : GetShoppingListResponse {
        $result = new GetShoppingListResponse();

        try {
            if (!isset($_SESSION['uid']))
                throw new Exception("Set failed: User in session does not exist.");

            /** @var User|null $creator */
            $creator = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));
            if (is_null($creator)) {
                $result->setData(null);
                throw new Exception("Set failed: Creator user does not exist in database");
            }

            /** @var User|null $assignee */
            $assignee = $this->userRepository->findOneBy((array('uid' => $inputData->getAssigneeUid())));

            /** @var ShoppingList|null $shoppingList */
            $shoppingList = $this->shoppingListRepository->findOneBy((array('id' => $inputData->getShoppingListId())));
            if (is_null($shoppingList)) {
                $result->setData(null);
                throw new Exception("Set failed: Shopping list does not exist in database");
            }

            $dueTime = null;
            if (!is_null($inputData->getDueTime()))
                $dueTime = $inputData->getDueTime()->setTimezone(new DateTimeZone('UTC'));

            $oldShoppingListAssigneeUid = $shoppingList->getAssignee();

            if ($_SESSION['uid'] == $shoppingList->getCreator()->getUid()) {
                $shoppingList->setTitle($inputData->getTitle());
                $shoppingList->setDescription($inputData->getDescription());
                $shoppingList->setDueTime($dueTime);
                $shoppingList->setCreator($creator);
                $shoppingList->setAssignee($assignee);
                $shoppingList->setShoppingListItems($inputData->getShoppingListItems(), $this->entityManager);
                $shoppingList->setState(new EntityStateEnum(EntityStateEnum::ACTIVE));

                $this->entityManager->persist($shoppingList);
                $this->entityManager->flush();
            }  else {
                $result->setData(null);
                throw new Exception("Set failed: User cannot update the shopping list");
            }

            if ($_SESSION['uid'] != $inputData->getAssigneeUid() && $oldShoppingListAssigneeUid != $inputData->getAssigneeUid()) {
                $this->notificationController->sendNotification(
                    $creator,
                    $assignee,
                    new NotificationTypeEnum(NotificationTypeEnum::ASSIGN_SHOPPING_LIST)
                );
            }

            $result->setData($creator->getCreatedShoppingLists());
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetShoppingListItemCheckedValue mutation
     *
     * Sets check value of shopping list item
     *
     * @Mutation(name="setShoppingListItemCheckedValue")
     *
     * @return GetShoppingListResponse
     */
    public function setShoppingListItemCheckedValue(int $shoppingListItemId, bool $shoppingListItemCheckedValue) : GetShoppingListResponse {
        $result = new GetShoppingListResponse();

        try {
            if (!isset($_SESSION['uid']))
                throw new Exception("Set failed: User in session does not exist.");

            /** @var ShoppingListItem|null $shoppingListItem */
            $shoppingListItem = $this->shoppingListItemRepository->findOneBy((array('id' => $shoppingListItemId)));
            if (is_null($shoppingListItem)) {
                $result->setData(null);
                throw new Exception("Set failed: Shopping list item does not exist in database");
            }

            if ($_SESSION['uid'] == $shoppingListItem->getShoppingList()->getCreator()->getUid() || $_SESSION['uid'] == $shoppingListItem->getShoppingList()->getAssignee()->getUid()) {
                $shoppingListItem->setChecked($shoppingListItemCheckedValue);

                $this->entityManager->persist($shoppingListItem);
                $this->entityManager->flush();
            }  else {
                $result->setData(null);
                throw new Exception("Set failed: User cannot update the shopping list");
            }

            $result->setData($shoppingListItem->getShoppingList()->getCreator()->getCreatedShoppingLists());
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }



}