<?php

namespace App\Application\Controllers\QueryControllers;

use App\Application\Model\Entities\Recipe;
use App\Application\Model\Entities\User;
use App\Application\Model\Entities\UserRecipe;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\NotificationTypeEnum;
use App\Application\Model\Enum\UserRecipeTypeEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateRecipeInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateTaskInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetRecipeInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetTaskInput;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetRecipeCategoriesResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetRecipeResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetTaskResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetUserResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\RemoveItemResponse;
use App\Application\Utils;
use Exception;
use Kreait\Firebase\Auth;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\Query;
use TheCodingMachine\GraphQLite\Annotations\UseInputType;

/**
 * Class RecipeController. Resolves queries and mutations connected with Recipe entity and appropriate other entities
 *
 * @package App\Application\Controllers\QueryControllers
 */
class RecipeController extends BaseController
{
    private $firebase;
    protected $notificationController;

    /**
     * TaskController constructor. Sets main repository of class @see User
     */
    public function __construct()
    {
        parent::__construct();
        $this->firebase = $this->container->get(Auth::class);
        $this->notificationController = new NotificationController();
    }

    /**
     * CreateRecipe mutation
     *
     * Creates new recipe
     *
     * @Mutation(name="createRecipe")
     *
     * @UseInputType(for="$inputData", inputType="CreateRecipeInput")
     * @param CreateRecipeInput $inputData
     * @return GetRecipeResponse
     */
    public function createRecipe(CreateRecipeInput $inputData) : GetRecipeResponse {
        $result = new GetRecipeResponse();

        try {
            if (!isset($_SESSION['uid']))
                throw new Exception("Create failed: User in session does not exist.");

            /** @var User|null $creator */
            $creator = $this->userRepository->findOneBy((array('uid' => $inputData->getCreator())));
            if (is_null($creator)) {
                $result->setData(null);
                throw new Exception("Create failed: User, who you can create a recipe does not exist in database");
            }

            $recipeCategories = array_map(function($id) {
                return $this->recipeCategoryRepository->find($id);
                }, empty($inputData->getRecipeCategories()) ? [] : $inputData->getRecipeCategories()
            );

            $newRecipe = Recipe::create(
                $inputData->getTitle(),
                $inputData->getServings(),
                $inputData->getCaloriesPerServing(),
                $inputData->getSource(),
                $inputData->getPreparationTime(),
                $inputData->getPreparationTimeUnit(),
                $inputData->getDifficulty(),
                $inputData->getDescription(),
                $inputData->getRecipeIngredients(),
                $inputData->getRecipeInstructions(),
                $recipeCategories,
                $this->entityManager
            );

            $newUserRecipe = UserRecipe::create(
                $creator,
                $newRecipe,
                false,
                null,
                null,
                new UserRecipeTypeEnum(UserRecipeTypeEnum::CREATOR),
                $inputData->isWillCook(),
                new EntityStateEnum((EntityStateEnum::ACTIVE))
            );

            $this->entityManager->persist($newRecipe);
            $this->entityManager->persist($newUserRecipe);
            $this->entityManager->flush();

            $result->setData($creator->getRecipes());
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetRecipe mutation
     *
     * Sets new data to given recipe
     *
     * @Mutation(name="setRecipe")
     *
     * @UseInputType(for="$inputData", inputType="SetRecipeInput")
     * @param SetRecipeInput $inputData
     * @return GetRecipeResponse
     */
    public function setRecipe(SetRecipeInput $inputData) : GetRecipeResponse {
        $result = new GetRecipeResponse();

        try {
            if (!isset($_SESSION['uid']))
                throw new Exception("Create failed: User in session does not exist.");

            /** @var User|null $creator */
            $creator = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));
            if (is_null($creator)) {
                $result->setData(null);
                throw new Exception("Create failed: User, does not exist in database");
            }

            /** @var UserRecipe|null $userRecipe */
            $userRecipe = $this->userRecipeRepository->findOneBy((array('id' => $inputData->getUserRecipeId())));
            if (is_null($userRecipe)) {
                $result->setData(null);
                throw new Exception("Create failed: User, who you can create a recipe does not exist in database");
            }

            /** @var Recipe|null $recipe */
            $recipe = $userRecipe->getRecipe();
            if (is_null($recipe)) {
                $result->setData(null);
                throw new Exception("Create failed: User, who you can create a recipe does not exist in database");
            }

            $recipeCategories = array_map(function($id) {
                return $this->recipeCategoryRepository->find($id);
            }, $inputData->getRecipeCategories());

            $recipe->setTitle($inputData->getTitle());
            $recipe->setServings($inputData->getServings());
            $recipe->setCaloriesPerServing($inputData->getCaloriesPerServing());
            $recipe->setSource($inputData->getSource());
            $recipe->setPreparationTime($inputData->getPreparationTime());
            $recipe->setPreparationTimeUnit($inputData->getPreparationTimeUnit());
            $recipe->setDifficulty($inputData->getDifficulty());
            $recipe->setDescription($inputData->getDescription());
            $recipe->setRecipeIngredients($inputData->getRecipeIngredients(), $this->entityManager);
            $recipe->setRecipeInstructions($inputData->getRecipeInstructions(), $this->entityManager);
            $recipe->setRecipeCategories($recipeCategories, $this->entityManager);
            $this->entityManager->flush();

            $userRecipe->setComment($inputData->getComment());
            $userRecipe->setRating($inputData->getRating());
            $userRecipe->setFavourite($inputData->isFavourite());
            $userRecipe->setWillCook($inputData->isWillCook());

            $this->entityManager->persist($recipe);
            $this->entityManager->persist($userRecipe);
            $this->entityManager->flush();

            $result->setData($creator->getRecipes());
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * DeleteUserRecipe mutation
     *
     * Makes given recipe deleted
     *
     * @Mutation(name="deleteUserRecipe")
     *
     * @param int $userRecipeId
     * @return RemoveItemResponse
     */
    public function deleteUserRecipe(int $userRecipeId) : RemoveItemResponse {
        $result = new RemoveItemResponse();

        try {
            if (!isset($_SESSION['uid']))
                throw new Exception('Delete failed: No uid in session');

            /** @var UserRecipe|null $userRecipe */
            $userRecipe = $this->userRecipeRepository->findOneBy((array('id' => $userRecipeId)));
            if (is_null($userRecipe)) {
                throw new Exception("Delete failed: Task does not exist in database");
            }

            $userRecipe->setState(new EntityStateEnum(EntityStateEnum::DELETED));
            $this->entityManager->persist($userRecipe);
            $this->entityManager->flush();

            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * ShareRecipe mutation
     *
     * Shares given recipe with given User
     * Creates new record to table UserRecipe based on given UserRecipe
     *
     * @Mutation(name="shareRecipe")
     *
     * @param int $userRecipeId
     * @param string $uid
     * @return GetRecipeResponse
     */
    public function shareRecipe(int $userRecipeId, string $uid) : GetRecipeResponse {
        $result = new GetRecipeResponse();

        try {
            if (!isset($_SESSION['uid']))
                throw new Exception('Share failed: No uid in session');

            /** @var UserRecipe|null $userRecipe */
            $userRecipe = $this->userRecipeRepository->findOneBy((array('id' => $userRecipeId)));
            if (is_null($userRecipe)) {
                throw new Exception("Delete failed: Task does not exist in database");
            }

            /** User who wants to share the recipe @var User|null $actualUser */
            $actualUser = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));
            if (is_null($actualUser) && $userRecipe->getUser() === $actualUser) {
                throw new Exception("Share failed: User does not exist in database");
            }

            /** User I want to share the recipe with @var User|null $newUser */
            $newUser = $this->userRepository->findOneBy((array('uid' => $uid)));
            if (is_null($newUser)) {
                throw new Exception("Share failed: User you want to share the recipe with does not exist in database");
            }

            $sharingRecipe= UserRecipe::create(
                $newUser,
                $userRecipe->getRecipe(),
                false,
                null,
                null,
                new UserRecipeTypeEnum(UserRecipeTypeEnum::VIEWER),
                false,
                new EntityStateEnum((EntityStateEnum::ACTIVE))
            );

            $this->entityManager->persist($sharingRecipe);
            $this->entityManager->flush();

            $this->notificationController->sendNotification(
                $actualUser,
                $newUser,
                new NotificationTypeEnum(NotificationTypeEnum::SHARE_RECIPE)
            );

            $result->setData($actualUser->getRecipes());
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * @Query(name="getRecipeCategories")
     *
     * Returns all predefined recipe categories from recipe_category table
     *
     * @return GetRecipeCategoriesResponse
     */
    public function getRecipeCategories() : GetRecipeCategoriesResponse {
        $result = new GetRecipeCategoriesResponse();

        try {
            if (!isset($_SESSION['uid']))
                throw new Exception('Error: No uid in session');

            $recipeCategories = $this->recipeCategoryRepository->findBy(array('parentRecipeCategory' => null));

            if (is_null($recipeCategories))
                throw new Exception("Something went wrong: Null exception. No categories were received from DB.");

            $result->setData($recipeCategories);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }
}