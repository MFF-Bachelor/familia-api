<?php


namespace App\Application\Controllers\QueryControllers;


use App\Application\Model\Entities\User;
use App\Application\Model\Entities\UserEmail;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateUserInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\UpdateUserInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\LoginUserInput;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetUserResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetUsersResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\RemoveItemResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\LoginUserResponse;
use DateTimeZone;
use Exception;
use InvalidArgumentException;
use Kreait\Firebase\Auth;
use Kreait\Firebase\Exception\Auth\UserNotFound;
use Kreait\Firebase\Exception\AuthException;
use Kreait\Firebase\Exception\FirebaseException;
use TheCodingMachine\GraphQLite\Annotations\Query;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\UseInputType;

/**
 * Class UserController. Resolves queries and mutations connected with User entity
 *
 * @package App\Application\Controllers\QueryControllers
 */
class UserController extends BaseController
{
    private $firebase;

    /**
     * UserController constructor. Sets main repository of class @see User
     */
    public function __construct()
    {
        parent::__construct();
        $this->firebase = $this->container->get(Auth::class);
    }

    /**
     * GetUser query - basic query resolver used for getting one user based on given uid or HTTP session
     *
     * @Query(name="getUser")
     *
     * @param string|null $uid unique id of user taken from firebase
     * @param string|null $email unique email of user taken from firebase
     * @return GetUserResponse
     */
    public function getUser(string $uid = null, string $email = null) : GetUserResponse {
        $result = new GetUserResponse();

        try {
            /** @var User $data */
            $data = null;
            if(!is_null($uid))
                $data = $this->userRepository->findOneBy((array('uid' => $uid)));
            else if (!is_null($email)) {
                //Test, if user does exist in Firebase
                $firebaseUser = $this->firebase->getUserByEmail($email);
                $data = $this->userRepository->findOneBy((array('uid' => $firebaseUser->uid)));
            }
            else if (isset($_SESSION['uid']))
                $data = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));
            else
                throw new Exception("No user in database with given parameters");

            $result->setData($data);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        } catch (AuthException | FirebaseException $exception) {
            $result->setData(null);
            $result->setSuccess(false);

            if($exception instanceof UserNotFound) {
                $result->setErrorMessage("User does not exist in Firebase");
            } else {
                $result->setErrorMessage($exception->getMessage());
            }
        }

        return $result;
    }

    /**
     * CreateUser mutation - basic query resolver used for create users
     *
     * @Mutation(name="createUser")
     *
     * @UseInputType(for="$inputData", inputType="CreateUserInput")
     *
     * @param CreateUserInput $inputData
     * @return GetUserResponse
     */
    public function createUser(CreateUserInput $inputData) : GetUserResponse
    {
        $result = new GetUserResponse();

        try {
            //Test, if user does exist in Firebase
            $firebaseUser = $this->firebase->getUser($inputData->getUid());

            /** @var User $foundUser */
            $foundUser = $this->userRepository->findOneBy((array('uid' => $inputData->getUid())));

            if (!is_null($foundUser) && ($foundUser->getState() != EntityStateEnum::DELETED)) {
                $result->setData($foundUser);
                throw new Exception("User already exists in database");
            }

            //all user emails (from firebase and from input)
            $emails = [];
            if (!is_null($inputData->getEmails()))
                $emails = $inputData->getEmails();
            array_push($emails, $firebaseUser->email);

            $birthDate = null;
            if (!is_null($inputData->getBirthDate()))
                $birthDate = $inputData->getBirthDate()->setTimezone(new DateTimeZone('UTC'));

            //Create new User entity
            $newUser = User::create(
                $inputData->getUid(),
                is_null($inputData->getState())
                    ? new EntityStateEnum(EntityStateEnum::ACTIVE)
                    : $inputData->getState(),
                $inputData->getFirstName(),
                $inputData->getLastName(),
                $emails,
                $inputData->getPhoneNumbers(),
                $birthDate,
                $inputData->getGender(),
                $inputData->getStreet(),
                $inputData->getDescriptiveNumber(),
                $inputData->getRoutingNumber(),
                $inputData->getPostcode(),
                $inputData->getCity(),
                $inputData->getCountry(),
                $this->entityManager
            );

            $this->entityManager->persist($newUser);
            $this->entityManager->flush();

            return $this->getUser($newUser->getUid());
        } catch (AuthException | FirebaseException $exception) {
            $result->setData(null);
            $result->setSuccess(false);

            if($exception instanceof UserNotFound) {
                $result->setErrorMessage("User does not exist in Firebase");
            } else {
                $result->setErrorMessage($exception->getMessage());
            }
        } catch (Exception $exception)  {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * UpdateUser mutation - basic query resolver used for updating users based on given pagination parameters
     *
     * @Mutation(name="updateUser")
     * @UseInputType(for="$inputData", inputType="UpdateUserInput")
     *
     * @param UpdateUserInput $inputData
     * @return GetUserResponse
     */
    public function updateUser(UpdateUserInput $inputData) : GetUserResponse {

        $result = new GetUserResponse();

        try {
            if (is_null($inputData->getUid()) && isset($_SESSION['uid'])) {
                $inputData->setUid($_SESSION['uid']);
            }

            //Test, if user does exist in Firebase
            $firebaseUser = $this->firebase->getUser($inputData->getUid());

            /** @var User|null $foundUser */
            $foundUser = $this->userRepository->findOneBy((array('uid' => $inputData->getUid())));

            if (is_null($foundUser)) {
                $result->setData(null);
                throw new Exception("Update failed: User does not exist in database");
            }

            //all user emails (from firebase and from input)
            /** @var UserEmail[] $emails */
            $emails = [];
            if (!is_null($inputData->getEmails()))
                $emails = $inputData->getEmails();
            array_push($emails, $firebaseUser->email);

            $birthDate = null;
            if (!is_null($inputData->getBirthDate()))
                $birthDate = $inputData->getBirthDate()->setTimezone(new DateTimeZone('UTC'));

            //update new values
            if(!is_null($inputData->getState())) $foundUser->setState($inputData->getState());
            if(!is_null($inputData->getFirstName())) $foundUser->setFirstName($inputData->getFirstName());
            if(!is_null($inputData->getLastName())) $foundUser->setLastName($inputData->getLastName());
            if(!is_null($inputData->getEmails())) $foundUser->setEmails($emails, $this->entityManager);
            if(!is_null($inputData->getPhoneNumbers())) $foundUser->setPhoneNumbers($inputData->getPhoneNumbers(), $this->entityManager);
            if(!is_null($inputData->getBirthDate())) $foundUser->setBirthDate($birthDate);
            if(!is_null($inputData->getGender())) $foundUser->setGender($inputData->getGender());
            if(!is_null($inputData->getStreet())) $foundUser->setStreet($inputData->getStreet());
            if(!is_null($inputData->getDescriptiveNumber())) $foundUser->setDescriptiveNumber($inputData->getDescriptiveNumber());
            if(!is_null($inputData->getRoutingNumber())) $foundUser->setRoutingNumber($inputData->getRoutingNumber());
            if(!is_null($inputData->getPostcode())) $foundUser->setPostcode($inputData->getPostcode());
            if(!is_null($inputData->getCity())) $foundUser->setCity($inputData->getCity());
            if(!is_null($inputData->getCountry())) $foundUser->setCountry($inputData->getCountry());

            //Update user in DB
            $this->entityManager->flush();

            //Update user in firebase
            $this->firebase->updateUser($inputData->getUid(), [
                "displayName" => $foundUser->getFirstName() . ' ' . $foundUser->getLastName()
            ]);

            $result->setData($foundUser);
            $result->setSuccess(true);
            $result->setErrorMessage("");

        } catch (AuthException | FirebaseException $exception) {
            $result->setData(null);
            $result->setSuccess(false);

            if($exception instanceof UserNotFound) {
                $result->setErrorMessage("Update failed: User does not exist in Firebase");
            } else {
                $result->setErrorMessage($exception->getMessage());
            }
        } catch (Exception $exception)  {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * LogoutUser mutation - basic query resolver used for logout user
     *
     * @Mutation(name="logoutUser")
     *
     * @return LoginUserResponse
     */
    public function logoutUser() : LoginUserResponse {
        $result = new LoginUserResponse();
        try {

            if(isset($_SESSION['loggedIn'])) {
                setcookie(session_name(), null, time() - 86400);
                session_destroy();
                $_SESSION = array();
            } else
                throw new Exception("Logout user failed");

            $result->setSuccess(true);
            $result->setErrorMessage("");
        } catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * LoginUser mutation - basic query resolver used for logout user
     *
     * @Mutation(name="loginUser")
     * @UseInputType(for="$inputData", inputType="LoginUserInput")
     *
     * @param LoginUserInput $inputData
     * @return GetUserResponse
     */
    public function loginUser(LoginUserInput $inputData) : GetUserResponse {
        $result = new GetUserResponse();

        try {
            //verify token get uid from firebase base on given token
            $uid = $this->firebase->verifyIdToken($inputData->getToken())->claims()->get("sub");

            /** @var User $foundUser */
            $foundUser = $this->userRepository->findOneBy((array('uid' => $uid)));


            if (is_null($foundUser) || ($foundUser->getState() == EntityStateEnum::DELETED)) {
                throw new Exception('User does not exists in database');
            } else {
                $result->setSuccess(true);
                $result->setErrorMessage("");
                $result->setData($foundUser);
            }

            $_SESSION['loggedIn'] = true;
            $_SESSION['uid'] = $uid;
            setcookie('PHPSESSID', session_id());


        } catch (InvalidArgumentException $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage('The token is invalid: ' . $exception->getMessage());
        } catch (Exception | AuthException | FirebaseException $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * DeleteUser mutation
     *
     * Makes given user deleted
     *
     * @Mutation(name="deleteUser")
     *
     * @param string $userUID
     * @return RemoveItemResponse
     */
    public function deleteUser(string $userUID) : RemoveItemResponse {
        $result = new RemoveItemResponse();

        try {
            $foundUser = $this->userRepository->findOneBy((array('uid' => $userUID)));
            if (is_null($foundUser)) {
                throw new Exception("Delete failed: User does not exist in database");
            }

            $foundUser->setState(new EntityStateEnum(EntityStateEnum::DELETED));

            $this->entityManager->flush();

            $result->setSuccess(true);
            $result->setErrorMessage("");

        } catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * DeleteUserFromFirebase mutation
     *
     * Removes given user from Firebase
     *
     * @Mutation(name="deleteUserFromFirebase")
     *
     * @param string $email
     * @return RemoveItemResponse
     */
    public function deleteUserFromFirebase(string $email) : RemoveItemResponse {
        $result = new RemoveItemResponse();

        try {
            $userUid = $this->firebase->getUserByEmail($email)->uid;

            $foundUser = $this->userRepository->findOneBy((array('uid' => $userUid)));
            if (!is_null($foundUser)) {
                throw new Exception("Delete failed: User exists in database");
            }

            $this->firebase->deleteUser($userUid);


            $result->setSuccess(true);
            $result->setErrorMessage("");

        } catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        } catch (AuthException | FirebaseException $exception) {
            $result->setSuccess(false);

            if($exception instanceof UserNotFound) {
                $result->setErrorMessage("User does not exist in Firebase");
            } else {
                $result->setErrorMessage($exception->getMessage());
            }
        }

        return $result;
    }

}
