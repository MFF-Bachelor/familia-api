<?php

namespace App\Application\Controllers\QueryControllers;

use App\Application\Model\Entities\Task;
use App\Application\Model\Entities\User;
use App\Application\Model\Entities\UserContact;
use App\Application\Model\Entities\UserRecipe;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateUserContactInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateRecipeInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetTaskInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetUserContactInput;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetTaskResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetUserContactResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetRecipeResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\RemoveItemResponse;
use Exception;
use Kreait\Firebase\Auth;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\UseInputType;

/**
 * Class RecipeController. Resolves queries and mutations connected with Recipe entity and appropriate other entities
 *
 * @package App\Application\Controllers\QueryControllers
 */
class ContactController extends BaseController
{
    private $firebase;
    protected $notificationController;

    /**
     * TaskController constructor. Sets main repository of class @see User
     */
    public function __construct()
    {
        parent::__construct();
        $this->firebase = $this->container->get(Auth::class);
        $this->notificationController = new NotificationController();
    }

    /**
     * CreateContact mutation
     *
     * @Mutation(name="createContact")
     *
     * @UseInputType(for="$inputData", inputType="CreateUserContactInput")
     * @param CreateUserContactInput $inputData
     * @return GetUserContactResponse
     */
    public function createContact(CreateUserContactInput $inputData) : GetUserContactResponse {
        $result = new GetUserContactResponse();

        try {
            if (!isset($_SESSION['uid']))
                throw new Exception("Create failed: User in session does not exist.");

            $userUid = null;

            //mutation is called from mobile
            if (is_null($inputData->getUserUid())) {
                $userUid = $_SESSION['uid'];
            } else { //mutation is called from api
                $userUid = $inputData->getUserUid();
            }

            /** @var User|null $user */
            $user = $this->userRepository->findOneBy((array('uid' => $userUid)));
            if (is_null($user)) {
                $result->setData(null);
                throw new Exception("Create failed: User does not exist in database");
            }

            $newUserContact = UserContact::create(
                $user,
                $inputData->getEmail(),
                $inputData->getPhoneNumber(),
                $inputData->getLabel(),
                $inputData->getName(),
                $inputData->getState()
            );

            $result->setData($user->getContacts());
            $result->setSuccess(true);
            $result->setErrorMessage("");

            $this->entityManager->persist($newUserContact);
            $this->entityManager->flush();
        } catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * DeleteUserContact mutation
     *
     * Makes given user contact deleted
     *
     * @Mutation(name="deleteUserContact")
     *
     * @param int $userContactId
     * @return RemoveItemResponse
     */
    public function deleteUserContact(int $userContactId) : RemoveItemResponse {
        $result = new RemoveItemResponse();

        try {
            if (!isset($_SESSION['uid']))
                throw new Exception('Delete failed: No uid in session');

            /** @var UserRecipe|null $userContact */
            $userContact = $this->userContactRepository->findOneBy((array('id' => $userContactId)));
            if (is_null($userContact)) {
                throw new Exception("Delete failed: Contact does not exist in database");
            }

            $userContact->setState(new EntityStateEnum(EntityStateEnum::DELETED));
            $this->entityManager->persist($userContact);
            $this->entityManager->flush();

            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetUserContact mutation
     *
     * Sets attributes of given contact
     *
     * @Mutation(name="setUserContact")
     *
     * @UseInputType(for="$inputData", inputType="SetUserContactInput")
     * @param SetUserContactInput $inputData
     * @return GetUserContactResponse
     */
    public function setUserContact(SetUserContactInput $inputData) : GetUserContactResponse {
        $result = new GetUserContactResponse();

        try {
            if (!isset($_SESSION['uid']))
                throw new Exception("Set failed: User in session does not exist.");


            /** @var UserContact|null $userContact */
            $userContact = $this->userContactRepository->findOneBy((array('id' => $inputData->getUserContactId())));
            if (is_null($userContact)) {
                throw new Exception("Set failed: Contact does not exist in database");
            }

            /** @var User|null $user */
            $user = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));
            if (is_null($user)) {
                $result->setData(null);
                throw new Exception("Create failed: User does not exist in database");
            }

            $userContact->setUser($user);
            $userContact->setEmail($inputData->getEmail());
            $userContact->setPhoneNumber($inputData->getPhoneNumber());
            $userContact->setLabel($inputData->getLabel());
            $userContact->setName($inputData->getName());
            $userContact->setState($inputData->getState());

            $this->entityManager->persist($userContact);
            $this->entityManager->flush();

            $result->setData($user->getOtherCreatedTasks());
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }
}