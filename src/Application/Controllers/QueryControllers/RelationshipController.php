<?php


namespace App\Application\Controllers\QueryControllers;

use App\Application\Model\Entities\InactiveUser;
use App\Application\Model\Entities\Relationship;
use App\Application\Model\Entities\User;
use App\Application\Model\Entities\UserEmail;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\GenderEnum;
use App\Application\Model\Enum\NotificationTypeEnum;
use App\Application\Model\Enum\RelationshipEnum;
use App\Application\Model\Enum\RelationshipSideEnum;
use App\Application\Model\Enum\RelationshipStateEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateOneWayRelationshipInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateTwoWayRelationshipInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\UpdateOneWayRelationshipInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\UpdateTwoWayRelationshipInput;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetRelationshipResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetRelationshipsResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetUserResponse;
use App\Application\Utils;
use DateTimeZone;
use Exception;
use Kreait\Firebase\Auth;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\Query;
use TheCodingMachine\GraphQLite\Annotations\UseInputType;

/**
 * Class RelationshipController. Resolves queries and mutations connected with Relationship entity
 *
 * @package App\Application\Controllers\QueryControllers
 */
class RelationshipController extends BaseController
{
    private $firebase;
    protected $notificationController;

    /**
     * RelationshipController constructor. Sets main repository of class @see User
     */
    public function __construct()
    {
        parent::__construct();
        $this->firebase = $this->container->get(Auth::class);
        $this->notificationController = new NotificationController();
    }

    /**
     * CreateTwoWayRelationship mutation
     *
     * @Mutation(name="createTwoWayRelationship")
     *
     * @UseInputType(for="$inputData", inputType="CreateTwoWayRelationshipInput")
     * @param CreateTwoWayRelationshipInput $inputData
     * @return GetRelationshipResponse
     */
    public function createTwoWayRelationship(CreateTwoWayRelationshipInput $inputData) : GetRelationshipResponse {
        $result = new GetRelationshipResponse();

        try {
            if (!isset($_SESSION['uid']))
                throw new Exception("Create failed: User in session does not exist.");

            /** @var User|null $userFrom */
            $userFrom = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));
            if (is_null($userFrom)) {
                $result->setData(null);
                throw new Exception("Create failed: User, who you can create a relationship does not exist in database");
            }

            /** @var UserEmail|null $userToEmail */
            $userToEmail = $this->userEmailRepository->findOneBy((array('email' => strtolower($inputData->getUserToEmail()))));
            if (is_null($userToEmail)) {
                $result->setData(null);
                throw new Exception("Create failed: User does not exist in database");
            }

            /** @var User|null $userTo */
            $userTo = $userToEmail->getUser();

            $isRelationshipLimitReached = $this->isRelationshipLimitReached($userFrom, $inputData->getRole(), $inputData->getGender());
            $isThereTwoWayRelationship = $this->isThereTwoWayRelationship($userFrom, $userTo);

            /** @var Relationship $relationship */
            $relationship = null;
            /** @var Relationship $oppositeRelationship */
            $oppositeRelationship = null;


            if ($isRelationshipLimitReached || $isThereTwoWayRelationship)
                throw new Exception("Create failed: Relationship already exists.");

            $relationship = $this->relationshipRepository->findOneBy(array('userFrom' => $userFrom, 'userTo' => $userTo));
            $oppositeRelationship = $this->relationshipRepository->findOneBy(array('userFrom' => $userTo, 'userTo' => $userFrom));

            if (!is_null($relationship)) {
                $relationship->setState(new RelationshipStateEnum(RelationshipStateEnum::ACTIVE));
                $oppositeRelationship->setState(new RelationshipStateEnum(RelationshipStateEnum::ACTIVE));
                $this->notificationController->sendNotification(
                    $userFrom,
                    $userTo,
                    new NotificationTypeEnum(NotificationTypeEnum::CONFIRM_RELATIONSHIP));

            } else {
                $relationship = Relationship::create(
                    $userFrom,
                    $userTo,
                    null,
                    new GenderEnum($inputData->getGender()),
                    $inputData->getBio(),
                    $inputData->getRole(),
                    new RelationshipStateEnum(RelationshipStateEnum::NOT_CONFIRMED),
                    $inputData->getSide() == null ? null : new RelationshipSideEnum($inputData->getSide())
                );

                $oppositeRelationship = Relationship::create(
                    $userTo,
                    $userFrom,
                    null,
                    new GenderEnum($userFrom->getGender()),
                    null,
                    new RelationshipEnum(Utils::GetOppositeRelationship($inputData->getRole())),
                    new RelationshipStateEnum(RelationshipStateEnum::TO_CONFIRM),
                    null
                );

                $this->notificationController->sendNotification(
                    $userFrom,
                    $userTo,
                    new NotificationTypeEnum(NotificationTypeEnum::CREATE_RELATIONSHIP)
                );
            }


            $this->entityManager->persist($relationship);
            $this->entityManager->persist($oppositeRelationship);
            $this->entityManager->flush();

            $result->setData(null);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * CreateOneWayRelationshipInput mutation
     *
     * @Mutation(name="createOneWayRelationship")
     *
     * @UseInputType(for="$inputData", inputType="CreateOneWayRelationshipInput")
     * @param CreateOneWayRelationshipInput $inputData
     * @return GetRelationshipResponse
     */
    public function createOneWayRelationship(CreateOneWayRelationshipInput $inputData) : GetRelationshipResponse {
        $result = new GetRelationshipResponse();

        try {
            if (!isset($_SESSION['uid']))
                throw new Exception("Create failed: User in session does not exist.");

            /** @var User|null $userFrom */
            $userFrom = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));
            if (is_null($userFrom)) {
                $result->setData(null);
                throw new Exception("Create failed: User, who you can create a newRelationship does not exist in database");
            }

            $isRelationshipLimitReached = $this->isRelationshipLimitReached($userFrom, $inputData->getRole(), $inputData->getGender());

            /** @var Relationship $newRelationship */
            $newRelationship = null;

            if ($isRelationshipLimitReached)
                throw new Exception("Create failed");

            $birthDate = null;
            if (!is_null($inputData->getBirthDate()))
                $birthDate = $inputData->getBirthDate()->setTimezone(new DateTimeZone('UTC'));

            $newInactiveUser = InactiveUser::create(
                new EntityStateEnum(EntityStateEnum::ACTIVE),
                $inputData->getFirstName(),
                $inputData->getLastName(),
                $birthDate,
                $inputData->getGender(),
                $this->entityManager
            );

            $this->entityManager->persist($newInactiveUser);
            $this->entityManager->flush();

            /** @var InactiveUser $newRelationship */
            $newRelationship = Relationship::create(
                $userFrom,
                null,
                $newInactiveUser,
                new GenderEnum($inputData->getGender()),
                $inputData->getBio(),
                new RelationshipEnum($inputData->getRole()),
                new RelationshipStateEnum(RelationshipStateEnum::ACTIVE),
                $inputData->getSide() == null ? null : new RelationshipSideEnum($inputData->getSide())
            );

            $this->entityManager->persist($newRelationship);
            $this->entityManager->flush();


            $result->setData(null);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * UpdateOneWayRelationship mutation
     *
     * @Mutation(name="updateOneWayRelationship")
     *
     * @UseInputType(for="$inputData", inputType="UpdateOneWayRelationshipInput")
     * @param UpdateOneWayRelationshipInput $inputData
     * @return GetRelationshipResponse
     */
    public function updateOneWayRelationship(UpdateOneWayRelationshipInput $inputData) : GetRelationshipResponse {
        $result = new GetRelationshipResponse();

        try {
            if (!isset($_SESSION['uid']))
                throw new Exception("Update failed: User in session does not exist.");

            /** @var User|null $userFrom */
            $userFrom = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));
            if (is_null($userFrom)) {
                $result->setData(null);
                throw new Exception("Update failed: User does not exist.");
            }

            /** @var Relationship|null $relationship */
            $relationship = $this->relationshipRepository->findOneBy((array('id' => $inputData->getRelationshipId())));
            if (is_null($relationship)) {
                $result->setData(null);
                throw new Exception("Update failed: relationship does not exist in database.");
            }

            if (!is_null($relationship->getUserTo()))
                throw new Exception("Update failed: Cannot update two way relationship");

            /** @var InactiveUser|null $inactiveUser */
            $inactiveUser = $this->inactiveUserRepository->findOneBy((array('id' => $relationship->getInactiveUserTo())));
            if (is_null($inactiveUser)) {
                $result->setData(null);
                throw new Exception("Update failed.");
            }

            $isRelationshipLimitReached = $this->isRelationshipLimitReached($userFrom, $inputData->getRole(), $inputData->getGender());

            if ((!$inputData->getRole()->equals($relationship->getRole()) || !$inputData->getGender()->equals($relationship->getUserToGender()))
                && $isRelationshipLimitReached)
                throw new Exception("Update failed: Limit of this type of relationship was reached.");

            $birthDate = null;
            if (!is_null($inputData->getBirthDate()))
                $birthDate = $inputData->getBirthDate()->setTimezone(new DateTimeZone('UTC'));

            if (!is_null($inputData->getFirstName())) $inactiveUser->setFirstName($inputData->getFirstName());
            if (!is_null($inputData->getLastName())) $inactiveUser->setLastName($inputData->getLastName());
            if (!is_null($inputData->getBirthDate())) $inactiveUser->setBirthDate($birthDate);
            if (!is_null($inputData->getGender())) $inactiveUser->setGender($inputData->getGender());
            if (!is_null($inputData->getBio())) $relationship->setBio($inputData->getBio());
            if (!is_null($inputData->getSide())) $relationship->setSide($inputData->getSide());
            if (!is_null($inputData->getRole())) $relationship->setRole($inputData->getRole());


            $this->entityManager->persist($inactiveUser);
            $this->entityManager->persist($relationship);
            $this->entityManager->flush();


            $result->setData(null);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * UpdateTwoWayRelationship mutation
     *
     * @Mutation(name="updateTwoWayRelationship")
     *
     * @UseInputType(for="$inputData", inputType="UpdateTwoWayRelationshipInput")
     * @param UpdateTwoWayRelationshipInput $inputData
     * @return GetRelationshipResponse
     */
    public function updateTwoWayRelationship(UpdateTwoWayRelationshipInput $inputData) : GetRelationshipResponse {
        $result = new GetRelationshipResponse();

        try {
            if (!isset($_SESSION['uid']))
                throw new Exception("Update failed: User in session does not exist.");

            /** @var User|null $userFrom */
            $userFrom = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));
            if (is_null($userFrom)) {
                $result->setData(null);
                throw new Exception("Update failed: User, who you can create a relationship does not exist in database");
            }

            /** @var Relationship|null $relationship */
            $relationship = $this->relationshipRepository->findOneBy((array('id' => $inputData->getRelationshipId())));
            if (is_null($relationship)) {
                $result->setData(null);
                throw new Exception("Update failed: Relationship does not exist in database");
            }

            if (is_null($relationship->getUserTo()) && !is_null($relationship->getInactiveUserTo()))
                throw new Exception("Update failed: Cannot update two way relationship");

            if (!is_null($inputData->getSide())) $relationship->setSide($inputData->getSide());
            if (!is_null($inputData->getBio())) $relationship->setBio($inputData->getBio());

            $this->entityManager->persist($relationship);
            $this->entityManager->flush();

            $result->setData(null);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * ConfirmRelationship mutation
     *
     * Confirms request for adding user to family
     *
     * @Mutation(name="confirmRelationship")
     *
     * @param int $relationshipId
     * @return GetRelationshipsResponse
     */
    public function confirmRelationship(int $relationshipId) : GetRelationshipsResponse {
        $result = new GetRelationshipsResponse();

        try {

            if (!isset($_SESSION['uid']))
                throw new Exception("Confirm failed: User in session does not exist.");

            /** @var Relationship $relationship */
            $relationship = $this->relationshipRepository->findOneBy(array('id' => $relationshipId));

            /** @var User|null $userFrom */
            $userFrom = $this->userRepository->findOneBy((array('id' => $relationship->getUserFrom())));
            if (is_null($userFrom)) {
                $result->setData(null);
                throw new Exception("Confirm failed: User does not exist in database");
            }

            /** @var User|null $userTo */
            $userTo = $this->userRepository->findOneBy((array('id' => $relationship->getUserTo())));
            if (is_null($userTo)) {
                $result->setData(null);
                throw new Exception("Confirm failed: User does not exist in database");
            }

            /** @var Relationship $oppositeRElationship */
            $oppositeRelationship = $this->relationshipRepository->findOneBy(array('userFrom' => $userTo, 'userTo' => $userFrom));

            $relationship->setState(new RelationshipStateEnum(RelationshipStateEnum::ACTIVE));
            $oppositeRelationship->setState(new RelationshipStateEnum(RelationshipStateEnum::ACTIVE));

            $this->entityManager->persist($relationship);
            $this->entityManager->persist($oppositeRelationship);
            $this->entityManager->flush();

            $result->setData($userFrom->getRelationshipsFrom());
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * RejectRelationship mutation
     *
     * Deprecated. Use DeleteUserRelationship.
     *
     * @Mutation(name="rejectRelationship")
     *
     * @param int $relationshipId
     * @return GetRelationshipsResponse
     */
    public function rejectRelationship(int $relationshipId) : GetRelationshipsResponse {
        $result = new GetRelationshipsResponse();

        try {
            if (!isset($_SESSION['uid']))
                throw new Exception("Confirm failed: User in session does not exist.");

            /** @var Relationship $relationship */
            $relationship = $this->relationshipRepository->findOneBy(array('id' => $relationshipId));

            /** @var User|null $userFrom */
            $userFrom = $this->userRepository->findOneBy((array('id' => $relationship->getUserFrom())));
            if (is_null($userFrom)) {
                $result->setData(null);
                throw new Exception("Confirm failed: User does not exist in database");
            }

            /** @var User|null $userTo */
            $userTo = $this->userRepository->findOneBy((array('id' => $relationship->getUserTo())));
            if (is_null($userTo)) {
                $result->setData(null);
                throw new Exception("Confirm failed: User does not exist in database");
            }

            /** @var Relationship $oppositeRElationship */
            $oppositeRelationship = $this->relationshipRepository->findOneBy(array('userFrom' => $userTo, 'userTo' => $userFrom));

            //$relationship->setState(new RelationshipStateEnum(RelationshipStateEnum::INACTIVE));
            //$oppositeRelationship->setState(new RelationshipStateEnum(RelationshipStateEnum::INACTIVE));

            $this->entityManager->remove($relationship);
            $this->entityManager->remove($oppositeRelationship);

            $this->entityManager->flush();

            $result->setData($userFrom->getRelationshipsFrom());
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * GetUserRelationships query
     *
     * @Query(name="getUserRelationships")
     *
     * @param string|null $uid
     * @return GetRelationshipsResponse
     */
    public function getUserRelationships(string $uid = null) : GetRelationshipsResponse {
        $result = new GetRelationshipsResponse();

        try {

            /** @var User|null $user */
            $user = $this->userRepository->findOneBy((array('uid' => $uid)));
            if (is_null($user)) {
                $result->setData(null);
                throw new Exception("Query failed: User does not exist in database");
            }

            $relationships = $user->getRelationshipsFrom();

            $result->setData($relationships);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception)  {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * DeleteUserRelationship mutation
     *
     * DeleteUserRelationship mutation is used for deleting relationships or for rejecting not-confirmed relationships
     *
     * @Mutation (name="deleteUserRelationship")
     *
     * @param int $relationshipId
     * @return GetUserResponse
     */
    public function deleteUserRelationship(int $relationshipId) : GetUserResponse {
        $result = new GetUserResponse();
        try {

            if (!isset($_SESSION['uid']))
                throw new Exception('Delete failed: No uid in session');

            /** @var User|null $user */
            $user = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));
            if (is_null($user)) {
                $result->setData(null);
                throw new Exception("Delete failed: User does not exist in database");
            }

            /** @var Relationship|null $relationship */
            $relationship = $this->relationshipRepository->findOneBy((array('id' => $relationshipId)));
            if (is_null($relationship)) {
                $result->setData(null);
                throw new Exception("Delete failed: Relationship does not exist in database");
            }

            //delete two-way relationship
            if (!is_null($relationship->getUserTo())) {
                $oppositeRelationship = $this->relationshipRepository->findOneBy(
                    array('userFrom' => $relationship->getUserTo(), 'userTo' => $relationship->getUserFrom())
                );
                $this->entityManager->remove($oppositeRelationship);
                if ($relationship->getState() == RelationshipStateEnum::TO_CONFIRM) {
                    $this->notificationController->sendNotification(
                        $user,
                        $relationship->getUserTo(),
                        new NotificationTypeEnum(NotificationTypeEnum::REJECT_RELATIONSHIP)
                    );
                } else  {
                    $this->notificationController->sendNotification(
                        $user,
                        $relationship->getUserTo(),
                        new NotificationTypeEnum(NotificationTypeEnum::DELETE_RELATIONSHIP)
                    );
                }
            //delete one-way relationship with appropriate inactiveUser
            } else if (!is_null($relationship->getInactiveUserTo())) {
                $inactiveUser = $this->inactiveUserRepository->findOneBy(array('id' => $relationship->getInactiveUserTo()->getId()));
                if (!is_null($inactiveUser))
                    $this->entityManager->remove($inactiveUser);
            }


            $this->entityManager->remove($relationship);
            $this->entityManager->flush();

            $result->setData($user);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception)  {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    private function isThereTwoWayRelationship(User $userFrom, User $userTo) {
        $qb1 = $this->relationshipRepository->createQueryBuilder('relationship');
        $qb1->andWhere('relationship.userFrom = :userFrom')
            ->setParameter('userFrom', $userFrom->getId())
            ->andWhere('relationship.userTo = :userTo')
            ->setParameter('userTo', $userTo)
            ->andWhere('relationship.state != :state')
            ->setParameter('state', RelationshipStateEnum::TO_CONFIRM);

        /** @var Relationship $existingRelationship */
        $existingRelationship =  $qb1->getQuery()->getResult();

        $qb1 = $this->relationshipRepository->createQueryBuilder('relationship');
        $qb1->select('relationship')
            ->andWhere('relationship.userFrom = :userFrom')
            ->setParameter('userFrom', $userTo)
            ->andWhere('relationship.userTo = :userTo')
            ->setParameter('userTo', $userFrom);

        /** @var Relationship $existingOppositeRelationship */
        $existingOppositeRelationship =  $qb1->getQuery()->getResult();

        if(!empty($existingRelationship) && !empty($existingOppositeRelationship))
            return true;

        return false;
    }


    // Returns false, if limit of concrete type of relationship was reached. Otherwise true
    private function isRelationshipLimitReached(User $user, RelationshipEnum $role, GenderEnum $gender) : bool
    {
        $relationshipLimit = $role->getLimit();
        if ($relationshipLimit == -1) //-1 <=> no limit for this role
            return false;


        $qb = $this->relationshipRepository->createQueryBuilder('relationship');
        $qb->select(array('relationship'))
            ->andWhere('relationship.userFrom = :userFrom')
            ->setParameter('userFrom', $user)
            ->andWhere('relationship.state != :state')
            ->setParameter('state', RelationshipStateEnum::TO_CONFIRM)
            ->andWhere('relationship.role = :role')
            ->setParameter('role', $role)
            ->andWhere('relationship.userToGender = :gender')
            ->setParameter('gender', $gender);

        /** @var Relationship $existingOppositeRelationship */
        $relationships =  $qb->getQuery()->getResult();

        if (count($relationships) < $relationshipLimit)
            return false;
        return true;
    }


}