<?php


namespace App\Application\Controllers\QueryControllers;


use App\Application\Model\Entities\FcmToken;
use App\Application\Model\Entities\Notification;
use App\Application\Model\Entities\SentNotification;
use App\Application\Model\Entities\User;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\NotificationTypeEnum;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetFcmTokenResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetRelationshipResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetUserResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GraphQLResponseModel;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\RemoveItemResponse;
use Exception;
use Kreait\Firebase\Messaging;
use Kreait\Firebase\Messaging\CloudMessage;
use TheCodingMachine\GraphQLite\Annotations\Mutation;

class NotificationController extends BaseController
{
    private $firebase;

    /**
     * UserController constructor. Sets main repository of class @see User
     */
    public function __construct()
    {
        parent::__construct();
        $this->firebase = $this->container->get(Messaging::class);
    }

    /**
     * SendFcmToken mutation
     *
     * There are two ways how to call this query
     * 1. fcmToken is given and oldFcmToken is null. In this case new FcmToken will be created in database.
     * 2. both fcmToken and oldFcmToken are given. In this case oldFcmToken will be replaced by fcmToken in database.
     *
     * @Mutation(name="sendFcmToken")
     *
     * @param string $fcmToken
     * @param string|null $oldFcmToken
     * @return GetFcmTokenResponse
     */
    public function sendFcmToken(string $fcmToken, string $oldFcmToken = null) : GetFcmTokenResponse {
        $result = new GetFcmTokenResponse();

        try {
            /** @var User $user */
            $user = null;
            if (!isset($_SESSION['uid'])) {
                throw new Exception("No user in database with given parameters.");
            }
            $user = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));


            /** @var FcmToken $dbFcmToken */
            $dbFcmToken = null;
            if (!is_null($oldFcmToken)) {
                $dbFcmToken = $this->fcmTokenRepository->findOneBy((array('fcmToken' => $oldFcmToken, 'user' => $user)));

                if (is_null($dbFcmToken)) {
                    throw new Exception("FcmToken for this user does not exist in database.");
                }

                $dbFcmToken->setFcmToken($fcmToken);
            } else {
                $dbFcmToken = FcmToken::create(
                    $user,
                    $fcmToken
                );
            }

            $this->entityManager->persist($dbFcmToken);
            $this->entityManager->flush();

            $result->setData($dbFcmToken);
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * @param User $userFrom
     * @param User $userTo
     * @param NotificationTypeEnum $notificationType
     */
    public function sendNotification(User $userFrom, User $userTo, NotificationTypeEnum $notificationType) {
        try {
            $newNotification = Notification::create(
                $userTo,
                $userFrom,
                $notificationType->getNotificationTitle(),
                $notificationType->getNotificationText($userFrom, $userTo),
                $notificationType,
                EntityStateEnum::ACTIVE,
                null
            );

            $this->entityManager->persist($newNotification);

            /** @var FcmToken $fcmTokens */
            $fcmTokens = $this->fcmTokenRepository->findBy(array('userId' => $userTo->getId()));

            if(!empty($fcmTokens)) {
                $message = CloudMessage::new()->withNotification([
                    'title' => $newNotification->getTitle(),
                    'body' => $newNotification->getText()
                ]);
                $sendReport = $this->firebase->sendMulticast($message, $fcmTokens);

                foreach ($sendReport->getItems() as $item) {
                    /** @var FcmToken $fcmToken */
                    $fcmToken = $this->fcmTokenRepository->findOneBy(array('fcmToken' => $item->target()->value()));

                    $errorMessage = null;
                    if (!is_null($item->error()))
                        $errorMessage = $item->error()->getMessage();

                    $dbSentNotification = SentNotification::create(
                        $fcmToken,
                        $newNotification,
                        $errorMessage
                    );

                    $this->entityManager->persist($dbSentNotification);
                }
            }
            $this->entityManager->flush();

        } catch (Exception $exception) {

        }
    }

    /**
     * DeleteNotification mutation
     *
     * Makes given notification deleted
     *
     * @Mutation(name="deleteNotification")
     *
     * @param int $notificationId
     * @return RemoveItemResponse
     */
    public function deleteNotification(int $notificationId) : RemoveItemResponse {
        $result = new RemoveItemResponse();

        try {
            if (!isset($_SESSION['uid']))
                throw new Exception('Delete failed: No uid in session');

            /** @var Notification|null $notification */
            $notification = $this->notificationRepository->findOneBy((array('id' => $notificationId)));
            if (is_null($notification)) {
                throw new Exception("Delete failed: Notification does not exist in database");
            }

            $notification->setState(new EntityStateEnum(EntityStateEnum::DELETED));
            $this->entityManager->persist($notification);
            $this->entityManager->flush();

            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }
}