<?php


namespace App\Application\Controllers\QueryControllers;

use App\Application\ApplicationDefaults;
use App\Application\Model\Entities\Notification;
use App\Application\Services\MediaService;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Slim\Factory\AppFactory;
use Doctrine\ORM\QueryBuilder;

/**
 * Class BaseController. Abstract controller class for resolving queries and mutations
 *
 * @package App\Application\Controllers\QueryControllers
 */
abstract class BaseController
{
    private $app;

    /**
     * Dependency container used across entire App
     */
    protected $container;

    /**
     * @var EntityManager
     *
     * Entity manager used across entire App
     */
    protected $entityManager;

    /** @var EntityRepository */
    protected $userRepository;
    /** @var EntityRepository */
    protected $relationshipRepository;
    /** @var EntityRepository */
    protected $inactiveUserRepository;
    /** @var EntityRepository */
    protected $notificationRepository;
    /** @var EntityRepository */
    protected $fcmTokenRepository;
    /** @var EntityRepository */
    protected $sentNotificationRepository;
    /** @var EntityRepository */
    protected $taskRepository;
    /** @var EntityRepository */
    protected $recipeRepository;
    /** @var EntityRepository */
    protected $userRecipeRepository;
    /** @var EntityRepository */
    protected $recipeCategoryRepository;
    /** @var EntityRepository */
    protected $userContactRepository;
    /** @var EntityRepository */
    protected $userEmailRepository;
    /** @var EntityRepository */
    protected $shoppingListRepository;
    /** @var EntityRepository */
    protected $shoppingListItemRepository;


    /**
     * @var MediaService Media service for handling medias
     */
    protected $mediaService;

    /**
     * BaseController constructor. Sets entity manager.
     */
    public function __construct()
    {
        $this->app = AppFactory::create();
        $this->container = $this->app->getContainer();
        $this->entityManager = $this->container->get(EntityManagerInterface::class);

        $this->userRepository = $this->entityManager->getRepository(ApplicationDefaults::$userTable);
        $this->relationshipRepository = $this->entityManager->getRepository(ApplicationDefaults::$relationshipTable);
        $this->inactiveUserRepository = $this->entityManager->getRepository(ApplicationDefaults::$inactiveUserTable);
        $this->notificationRepository = $this->entityManager->getRepository(ApplicationDefaults::$notificationTable);
        $this->fcmTokenRepository = $this->entityManager->getRepository(ApplicationDefaults::$fcmTokenTable);
        $this->sentNotificationRepository = $this->entityManager->getRepository(ApplicationDefaults::$sentNotificationTable);
        $this->taskRepository = $this->entityManager->getRepository(ApplicationDefaults::$taskTable);
        $this->recipeRepository = $this->entityManager->getRepository(ApplicationDefaults::$recipeTable);
        $this->userRecipeRepository = $this->entityManager->getRepository(ApplicationDefaults::$userRecipeTable);
        $this->recipeCategoryRepository = $this->entityManager->getRepository(ApplicationDefaults::$recipeCategoryTable);
        $this->userContactRepository = $this->entityManager->getRepository(ApplicationDefaults::$userContactTable);
        $this->userEmailRepository = $this->entityManager->getRepository(ApplicationDefaults::$userEmailTable);
        $this->shoppingListRepository = $this->entityManager->getRepository(ApplicationDefaults::$shoppingListTable);
        $this->shoppingListItemRepository = $this->entityManager->getRepository(ApplicationDefaults::$shoppingListItemTable);

        $this->mediaService = new MediaService();
    }
}