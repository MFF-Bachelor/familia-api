<?php

namespace App\Application\Controllers\QueryControllers;

use App\Application\Model\Entities\InactiveUser;
use App\Application\Model\Entities\Notification;
use App\Application\Model\Entities\Relationship;
use App\Application\Model\Entities\Task;
use App\Application\Model\Entities\User;
use App\Application\Model\Enum\EntityStateEnum;
use App\Application\Model\Enum\GenderEnum;
use App\Application\Model\Enum\NotificationTypeEnum;
use App\Application\Model\Enum\RelationshipEnum;
use App\Application\Model\Enum\RelationshipSideEnum;
use App\Application\Model\Enum\RelationshipStateEnum;
use App\Application\Model\Enum\TaskStateEnum;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateOneWayRelationshipInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateTaskInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\CreateTwoWayRelationshipInput;
use App\Application\Model\GraphQLTypes\GraphQLInputTypes\SetTaskInput;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\GetTaskResponse;
use App\Application\Model\GraphQLTypes\GraphQLResponseModels\RemoveItemResponse;
use App\Application\Utils;
use DateTimeZone;
use Exception;
use Kreait\Firebase\Auth;
use TheCodingMachine\GraphQLite\Annotations\Mutation;
use TheCodingMachine\GraphQLite\Annotations\Query;
use TheCodingMachine\GraphQLite\Annotations\UseInputType;

/**
 * Class TaskController. Resolves queries and mutations connected with Task entity
 *
 * @package App\Application\Controllers\QueryControllers
 */
class TaskController extends BaseController
{
    private $firebase;
    protected $notificationController;

    /**
     * TaskController constructor. Sets main repository of class @see User
     */
    public function __construct()
    {
        parent::__construct();
        $this->firebase = $this->container->get(Auth::class);
        $this->notificationController = new NotificationController();
    }

    /**
     * CreateTask mutation
     *
     * @Mutation(name="createTask")
     *
     * @UseInputType(for="$inputData", inputType="CreateTaskInput")
     * @param CreateTaskInput $inputData
     * @return GetTaskResponse
     */
    public function createTask(CreateTaskInput $inputData) : GetTaskResponse {
        $result = new GetTaskResponse();

        try {
            if (!isset($_SESSION['uid']))
                throw new Exception("Create failed: User in session does not exist.");

            /** @var User|null $creator */
            $creator = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));
            if (is_null($creator)) {
                $result->setData(null);
                throw new Exception("Create failed: User, who you can create a relationship does not exist in database");
            }

            for ($iterator = 0; $iterator < count($inputData->getAssigneeUids()); $iterator++) {
                /** @var User|null $assignee */
                $assignee = $this->userRepository->findOneBy((array('uid' => $inputData->getAssigneeUids()[$iterator])));
                if (is_null($assignee)) {
                    $result->setData(null);
                    throw new Exception("Create failed: User does not exist in database");
                }

                $canAssigneeEdit = $inputData->isCanAssigneeEdit();
                if ($assignee->getUid() == $creator->getUid())
                    $canAssigneeEdit = true;

                $dueTime = null;
                if (!is_null($inputData->getDueTime()))
                    $dueTime = $inputData->getDueTime()->setTimezone(new DateTimeZone('UTC'));

                $newTask = Task::create(
                    $inputData->getTitle(),
                    $inputData->getText(),
                    $dueTime,
                    $creator,
                    $assignee,
                    $inputData->getPriority(),
                    new TaskStateEnum(TaskStateEnum::ACTIVE),
                    $canAssigneeEdit
                );

                $this->entityManager->persist($newTask);
                $this->entityManager->flush();

                if ($creator->getUid() != $assignee->getUid()) {
                    $this->notificationController->sendNotification(
                        $creator,
                        $assignee,
                        new NotificationTypeEnum(NotificationTypeEnum::ASSIGN_TASK)
                    );
                }
            }

            $result->setData($creator->getOtherCreatedTasks());
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * DeleteTask mutation
     *
     * Makes given task deleted
     *
     * @Mutation(name="deleteTask")
     *
     * @param int $taskId
     * @return RemoveItemResponse
     */
    public function deleteTask(int $taskId) : RemoveItemResponse {
        $result = new RemoveItemResponse();

        try {
            if (!isset($_SESSION['uid']))
                throw new Exception('Delete failed: No uid in session');

            /** @var Task|null $task */
            $task = $this->taskRepository->findOneBy((array('id' => $taskId)));
            if (is_null($task)) {
                throw new Exception("Delete failed: Task does not exist in database");
            }

            $task->setState(new TaskStateEnum(TaskStateEnum::DELETED));
            $this->entityManager->persist($task);
            $this->entityManager->flush();

            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

    /**
     * SetTask mutation
     *
     * @Mutation(name="setTask")
     *
     * @UseInputType(for="$inputData", inputType="SetTaskInput")
     * @param SetTaskInput $inputData
     * @return GetTaskResponse
     */
    public function setTask(SetTaskInput $inputData) : GetTaskResponse {
        $result = new GetTaskResponse();

        try {
            if (!isset($_SESSION['uid']))
                throw new Exception("Set failed: User in session does not exist.");

            /** @var User|null $creator */
            $creator = $this->userRepository->findOneBy((array('uid' => $_SESSION['uid'])));
            if (is_null($creator)) {
                $result->setData(null);
                throw new Exception("Set failed: Creator user does not exist in database");
            }

            /** @var User|null $assignee */
            $assignee = $this->userRepository->findOneBy((array('uid' => $inputData->getAssigneeUid())));
            if (is_null($assignee)) {
                $result->setData(null);
                throw new Exception("Set failed: Assignee user does not exist in database");
            }

            /** @var Task|null $task */
            $task = $this->taskRepository->findOneBy((array('id' => $inputData->getTaskId())));
            if (is_null($task)) {
                $result->setData(null);
                throw new Exception("Set failed: Task does not exist in database");
            }

            $dueTime = null;
            if (!is_null($inputData->getDueTime()))
                $dueTime = $inputData->getDueTime()->setTimezone(new DateTimeZone('UTC'));

            $oldTaskAssigneeUid = $task->getAssignee();

            //If logged user is creator (owner) of the task, he CAN update the task always OR
            //logged user is not the owner so logged user must allowed to update the task
            if ($_SESSION['uid'] == $task->getCreator()->getUid() || $task->getCanAssigneeEdit()) {
                $task->setTitle($inputData->getTitle());
                $task->setText($inputData->getText());
                $task->setDueTime($dueTime);
                $task->setCreator($creator);
                $task->setAssignee($assignee);
                $task->setPriority($inputData->getPriority());
                $task->setState($inputData->getState());
                $task->setCanAssigneeEdit($inputData->isCanAssigneeEdit());

                $this->entityManager->persist($task);
                $this->entityManager->flush();
            } else if (!$task->getCanAssigneeEdit() && $inputData->getState() !== $task->getState()) {
                $task->setState($inputData->getState());
                $this->entityManager->persist($task);
                $this->entityManager->flush();
                $this->notificationController->sendNotification(
                    $assignee,
                    $task->getCreator(),
                    new NotificationTypeEnum(NotificationTypeEnum::DONE_TASK)
                );
            } else {
                $result->setData(null);
                throw new Exception("Set failed: User cannot update the task");
            }

            if ($_SESSION['uid'] != $inputData->getAssigneeUid() && $oldTaskAssigneeUid != $inputData->getAssigneeUid())
            {
                $this->notificationController->sendNotification(
                    $creator,
                    $assignee,
                    new NotificationTypeEnum(NotificationTypeEnum::ASSIGN_TASK)
                );
            }

            $result->setData($assignee->getOtherCreatedTasks());
            $result->setSuccess(true);
            $result->setErrorMessage("");
        }
        catch (Exception $exception) {
            $result->setData(null);
            $result->setSuccess(false);
            $result->setErrorMessage($exception->getMessage());
        }

        return $result;
    }

}