<?php

function firebaseLoginResolver($token, $auth): string
{
    $verifiedIdToken = $auth->verifyIdToken($token);

    // retrieve a stable UID, this can be used to identify the user in our database
    return $verifiedIdToken->claims()->get("sub");
}

function getEmailFromFirebase($uid, $auth): string {
    $user = $auth->getUser($uid);

    return  $user->email;
}
