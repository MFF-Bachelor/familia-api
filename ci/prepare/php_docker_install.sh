#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git and other dependencies (the php image doesn't have it) which is required by composer
apt-get update -yqq
apt-get upgrade -yqq
apt-get install -yqq --no-install-recommends git wget git zip unzip curl libcurl4-openssl-dev libmagickwand-dev

# Install composer
wget https://composer.github.io/installer.sig -O - -q | tr -d '\n' > installer.sig
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === file_get_contents('installer.sig')) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php'); unlink('installer.sig');"

# Install and enable imagick
printf "\n" | pecl -vvv install imagick
docker-php-ext-enable imagick