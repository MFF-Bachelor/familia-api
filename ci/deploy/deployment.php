<?php
// Top level config
$config = [
    'fullCliLog' => ["http", "exception"],
    'log' => __DIR__.'/../output/deployment.log',
];

$sections = require 'deployment.sections.php';

// Fill your own needs, this is example from my personal Laravel project
$ignore = [
    '.git*',
    '/ci'
];

$purge = [
    'var/cache/'
];

foreach ($sections as $name => $settings) {
    $config[$name] = array_merge($settings, [
        'ignore' => $ignore,
        'local' => '../../',
        'fileOutputDir' => '../output/',
    ]);
}

return $config;
