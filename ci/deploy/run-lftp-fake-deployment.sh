#!/bin/bash

lftp --user $FTP_USERNAME --env-password $FTP_SERVER <<LFTP_COMMANDS
cd $FTP_DIRECTORY;

set log:enabled yes;
set log:file ./ci/output/lftp.log;
set log:level 6;

put -c .htdeployment

bye;
LFTP_COMMANDS
